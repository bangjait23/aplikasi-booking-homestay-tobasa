/*
SQLyog Community v12.5.1 (64 bit)
MySQL - 10.1.30-MariaDB : Database - homestay
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`homestay` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `homestay`;

/*Table structure for table `admin_homestays` */

DROP TABLE IF EXISTS `admin_homestays`;

CREATE TABLE `admin_homestays` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_admin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kontak` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `admin_homestays` */

/*Table structure for table `admins` */

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kontak` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `admins` */

/*Table structure for table `homestays` */

DROP TABLE IF EXISTS `homestays`;

CREATE TABLE `homestays` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_homestay` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_kamar` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `area_terdekat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fasilitas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar_toilet` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_homestay` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Kontak` bigint(20) NOT NULL,
  `no_rekening` int(11) NOT NULL,
  `owner` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `homestays` */

insert  into `homestays`(`id`,`nama_homestay`,`alamat`,`jumlah_kamar`,`harga`,`area_terdekat`,`gambar`,`fasilitas`,`gambar_toilet`,`status_homestay`,`Kontak`,`no_rekening`,`owner`,`created_at`,`updated_at`) values 
(2,'Mawar Homestay','Lumban Bulbul, Balige, Kabupaten Tobasa, Sumatera Utara',2,400000,'Dekat dengan Pantai Bul Bul ±100m, 5 kamar, ada dapur,    maximal 50 orang, ruang tamu besar, dekat dengan sawah.','mhkXONWQih8IFLNhNa6RfbbIfkRGT8Aj4Crs7MhI.jpeg','Tempat Tidur\r\nMeja\r\nhanduk\r\nSelimut\r\nBantal','W4nP5fX7okw2Yq6UOCemcADNlyy1y2233zCZPb9X.png','Kosong',81262939209,1247728,'marlinasimangunsong@gmail.com','2019-06-11 05:21:33','2019-06-11 05:21:33'),
(3,'Aster Homestay','Lumban Bulbul, Balige, Kabupaten Tobasa, Sumatera Utara',2,400000,'-100 m ke Museum Batak TB Center\r\n  - 100 m ke Makam Sisingamangaraja\r\n  - 500 m ke Cafe Hollywood, Hotel Ita Soposurung','dKsPfgCdX7wHbovWoaLG9S0gsAXCVrAao1ggdmvp.jpeg','- Handuk kecil\r\n- Aqua gelas\r\n- Public WiFi\r\n- Termos air panas\r\n- TV','oTMhNwbSPGFiZH5FfRGpNeCaCywlo4tJbvEFMdoL.png','Kosong',81280652387,1247728,'aster@gmail.com','2019-06-11 05:33:49','2019-06-11 05:33:49'),
(4,'Ita Homestay','Lumban Bulbul, Balige, Kabupaten Tobasa, Sumatera Utara',2,400000,'- 100 m ke Museum Batak TB Center\r\n  - 100 m ke Makam Sisingamangaraja\r\n  - 500 m ke Cafe Hollywood, Hotel Ita Soposurung','4pYPChyzmYCON8xSwccxTBTHtWIlVJqOTCJTcGlv.jpeg','- Handuk kecil\r\n					  - Aqua gelas\r\n					  - Public WiFi\r\n 					  - Termos air panas\r\n 					  - TV','0YGgq1HJk86XXJDvCtnU6CSjpC9hg3CLmUvWILvQ.jpeg','Kosong',82123848944,1247728,'rosita_silalahi@yahoo.com','2019-06-11 05:39:26','2019-06-11 05:39:26');

/*Table structure for table `kamars` */

DROP TABLE IF EXISTS `kamars`;

CREATE TABLE `kamars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_homestay` int(11) NOT NULL,
  `no_kamar` int(11) NOT NULL,
  `jumlah_orang` int(11) NOT NULL,
  `nama_homestay` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_kamar` int(11) NOT NULL,
  `gambar_kamar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `kamars` */

insert  into `kamars`(`id`,`id_homestay`,`no_kamar`,`jumlah_orang`,`nama_homestay`,`harga_kamar`,`gambar_kamar`,`owner`,`status`,`created_at`,`updated_at`) values 
(1,1,1,3,'Polado Homestay',200000,'qkRsKBKwZWMFUyy37vSMlos7ozuMovHZ0ty2vAhk.jpeg','pernadi@gmail.com','Available','2019-06-11 04:16:28','2019-06-11 04:16:28'),
(2,2,1,2,'Mawar Homestay',150000,'6Vl5KXlkPymYSMtfcSERjdHKnVzLqkA6bKB9dkea.png','marlinasimangunsong@gmail.com','Available','2019-06-11 05:22:06','2019-06-11 05:22:06'),
(3,2,2,2,'Mawar Homestay',150000,'WxAZb7LoXuSdZ9OoU5VWO1gOmHE95c5krvN0kckv.png','marlinasimangunsong@gmail.com','Available','2019-06-11 05:22:40','2019-06-11 05:22:40'),
(4,3,1,3,'Aster Homestay',150000,'LsOoJnuKmWtlH4rQn4bC0xKIPLY3ofdTfc9DEQkU.png','aster@gmail.com','Available','2019-06-11 05:34:29','2019-06-11 05:34:29'),
(5,3,2,3,'Aster Homestay',150000,'MBhUp4DYdfzmgmqx1wdE2F4Yyt9VQuQNcOsWGTay.jpeg','aster@gmail.com','Available','2019-06-11 05:34:45','2019-06-11 05:34:45'),
(6,4,1,2,'Ita Homestay',100000,'TklO2zAJsF3SdNTlRAeZxEZ0aoQ88U4I7JMYNRhj.jpeg','rosita_silalahi@yahoo.com','Available','2019-06-11 05:40:05','2019-06-11 05:40:05'),
(7,4,2,2,'Ita Homestay',100000,'xCtFJWGXnmlYYregvA4mmjdfgogqBu4A96SReqk4.jpeg','rosita_silalahi@yahoo.com','Available','2019-06-11 05:40:24','2019-06-11 05:40:24');

/*Table structure for table `kritik_sarans` */

DROP TABLE IF EXISTS `kritik_sarans`;

CREATE TABLE `kritik_sarans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_homestay` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kritik` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saran` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `kritik_sarans` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2019_03_28_173249_create_admin_homestays_table',1),
(4,'2019_03_30_132338_create_wisatas_table',1),
(5,'2019_03_30_132553_create_homestays_table',1),
(6,'2019_05_14_125742_create_pemensanans_table',1),
(7,'2019_05_20_104450_create_kritik_sarans_table',1),
(8,'2019_05_22_114518_create_admins_table',1),
(9,'2019_06_04_091152_create_kamars_table',1),
(10,'2019_06_07_152547_create_pesan_kamars_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `pemensanans` */

DROP TABLE IF EXISTS `pemensanans`;

CREATE TABLE `pemensanans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_homestay` int(11) NOT NULL,
  `nama_pemesan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_checkin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_checkout` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(11) NOT NULL,
  `nama_homestay` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `pemensanans` */

/*Table structure for table `pesan_kamars` */

DROP TABLE IF EXISTS `pesan_kamars`;

CREATE TABLE `pesan_kamars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_pemesan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_homestay` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_homestay` int(11) NOT NULL,
  `no_kamar` int(11) NOT NULL,
  `id_kamar` int(11) NOT NULL,
  `total_pembayaran` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_masuk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_keluar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_pemesan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `pesan_kamars` */

insert  into `pesan_kamars`(`id`,`nama_pemesan`,`nama_homestay`,`id_homestay`,`no_kamar`,`id_kamar`,`total_pembayaran`,`status`,`tanggal_masuk`,`tanggal_keluar`,`username_pemesan`,`owner`,`created_at`,`updated_at`) values 
(5,'gaba','Polado Homestay',1,1,1,400000,'Check out','2019-6-13','2019-6-13','gaba','pernadi@gmail.com',NULL,NULL),
(6,'Gabriel','Polado Homestay',1,1,1,400000,'Check out','2019-6-13','2019-6-15','gaba','pernadi@gmail.com',NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`status`,`remember_token`,`created_at`,`updated_at`) values 
(1,'Gabriel Benni Pernadi Panjaitan','benni.pernadi@gmail.com',NULL,'$2y$10$0Wb6gPukfTul4e6px3CuMOdQ5GJN9pKtMAU7YBeCsHzoKzJD2XqiO','Admin','XkcdHpXphnBPtA6xk1djIg5d4w16G0gd6mi0qU5wL2urqRdSzywq26yiuJgr','2019-06-11 04:14:38','2019-06-11 04:14:38'),
(2,'Pernadi','pernadi@gmail.com',NULL,'$2y$10$bkfJqjdPVfu4rLtdXRXoSueoMkuCohDR2xJRdUeGma3R2663E0/CC','Owner','wRPcbicYGngQWEJc6ALUKTyduvaGjoNBoswAuRL8WZktoY6O9T38Pqqk9pe3','2019-06-11 04:14:55','2019-06-11 04:14:55'),
(3,'Mawar Homestay','marlinasimangunsong@gmail.com',NULL,'$2y$10$.LcMuNknq/sozTmnlxlLReSJ//rhRXe8nLFXfe.YoV1n4MU7jxKvC','Owner','EhaVvLubawFNsMfb9bnaCr8hjnVcNjSApLWPqrnOP20JDAhSkN2ED3dLbudy','2019-06-11 05:15:27','2019-06-11 05:15:27'),
(4,'Aster Homestay','aster@gmail.com',NULL,'$2y$10$Guw/404ihHOAmz0yU8MiFOUmQdB90HtfgUySCp0SlisgOw10FRVoe','Owner','2qFtyFMJr8ds9rfvWFAqyvKiX6hboMP24G3ecv7bCcofgZpUoadUOLxOWfSP','2019-06-11 05:24:58','2019-06-11 05:24:58'),
(5,'Ici homestay','ici@gmail.com',NULL,'$2y$10$MBOK2QQjHu6gnWF.bVERc.Xii2EBobdMouw.vxkVdV5Nx8NihW9DO','Owner','ikLNBMum36lb25DTPeLGFjbUXZdWQNjURIPStJjJfjNCrs8G5m17K00TrI6p','2019-06-11 05:25:45','2019-06-11 05:25:45'),
(6,'Ita Homestay','rosita_silalahi@yahoo.com',NULL,'$2y$10$qFVnRRqHuVCYEh9wB1dmzOpEa9.qaH/ENYZBn2EdIfUrKuwQnRGuO','Owner','i7o42MDw7kf8hfgBh80n9axSZdufgZX65G9zZ9preuaEUQ5GQxtUGoZFaaRP','2019-06-11 05:26:50','2019-06-11 05:26:50'),
(7,'Labbas Homestay','fridahildasimangunsong@yahoo.co.id',NULL,'$2y$10$O3V4A4cHONQ9WPjXbimXeuE/1fkgicCe53LrfleE8B8MdQK0Kgctq','Owner',NULL,'2019-06-11 05:27:57','2019-06-11 05:27:57');

/*Table structure for table `wisatas` */

DROP TABLE IF EXISTS `wisatas`;

CREATE TABLE `wisatas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_wisata` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `wisatas` */

insert  into `wisatas`(`id`,`nama_wisata`,`alamat`,`deskripsi`,`gambar`,`created_at`,`updated_at`) values 
(1,'Pemandian Bukit Gibeon','Desa Parsaoran Sibisa, Kecamatan Ajibata, Kabupaten Toba Samosir, Sumatera Utara','Pusat Seminar Bukit Gibeon sebenarnya merupakan tempat sekolah Misionaris/ sekolah Pendeta','mJP2P8kZ85dGz3VayneGjkoCrZuq8LMOXjJxTnp1.jpeg','2019-06-11 04:58:33','2019-06-11 04:58:33'),
(2,'Air Terjun Situmurun Binangalom','Desa Binangalom, Kecamatan Lumban Julu, Kabupaten Toba Samosir, Sumatera Utara.','Air Terjun Situmurun ini menghabiskan waktu sekitar 2 sampai 3 jam perjalanan','AYntzxwCeZmlnoghpDorzTMerQC107Ne7NLQ3cjq.jpeg','2019-06-11 05:01:04','2019-06-11 05:01:04'),
(3,'Pantai Bul-Bul','Lumban Bulbul, Balige, Kabupaten Toba Samosir, Sumatera Utara','Pantai Bulbul merupakan salah satu objek wisata dikawasan Kabupaten Toba Samosir yang menyedikan berbagai fasilitas bermain serta wahana menyenangkan yang disukai oleh banyak orang','yUcV2Z53bieKXA9tVZmwwoxigeETsU30BP8kgyP3.jpeg','2019-06-11 05:03:42','2019-06-11 05:03:42'),
(4,'Air Terjun Simanimbo','Ambar Halim, Pintu Pohan Merant, Kabupaten Toba Samosir, Sumatera Utara 22384','Air Terjun Simanimbo adalah salah satu air terjun yang aliran airnya terjun langsung ke dalam Danau Bendungan Sigura-Gura.','E9u034AhZMMvQS4O6wbdipJbQijepwKMF4aYblQj.jpeg','2019-06-11 05:06:20','2019-06-11 05:06:20'),
(5,'Bukit Tarabunga','Jl. Pagar Batu No. 88, Desa Silalahi, Kecamatan Balige, Kabupaten Toba Samosir, Provinsi Sumatera Utara','Mereka datang ke sini untuk menikmati sawah dan pemandangan di sekitarnya. Itu sama saja dengan alam yang ada di Ubud. Dari buki','v5JYGisKNgi6kxy1ekXdbE1OZIjacWuYFXRRJEQ6.jpeg','2019-06-11 05:07:22','2019-06-11 05:07:22'),
(6,'Museum T. B. Silalahi Center','Jl. Pagar Batu No. 88, Desa Silalahi, Kecamatan Balige, Kabupaten Toba Samosir, Provinsi Sumatera Utara','Museum TB. Silalahi adalah yayasan nirlaba yang didirikan oleh Letjen TNI (Purn) Dr. Tiopan Bernhard Silalahi. Yayasan ini didirikan dengan tujuan untuk melestarikan budaya Batak','cY2BAXg8dZK7OzpBPC9LyXnFCW6MUjzJVFu8tOpc.jpeg','2019-06-11 05:08:16','2019-06-11 05:08:16'),
(7,'Air Terjun Sigura-Gura',': Halado, Pintu Pohan Merant, Kabupaten Toba Samosir, Sumatera Utara 21274','Air terjun ini merupakan air terjun tertinggi di Indonesia dengan ketinggian mencapai 250 meter.','S0wIyH6ITHCytedDZBE8gi9YQy1eLICfXiCue2D5.jpeg','2019-06-11 05:09:22','2019-06-11 05:09:22'),
(8,'Pemandian Bukit Gibeon','Desa Parsaoran Sibisa, Kecamatan Ajibata, Kabupaten Toba Samosir, Sumatera Utara','Air Terjun','ES9pRRpdyYQm2JYYf3jUQwJGeZtDF2mhTF6I0N2U.jpeg','2019-06-11 08:19:25','2019-06-11 08:19:25');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
