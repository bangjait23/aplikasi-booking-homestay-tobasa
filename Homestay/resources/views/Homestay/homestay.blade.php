@extends('layouts.app')

@section('content')

<style media="screen">
  #navside{
    background-color:  #32EF7E;
  }
  #container{
    width: 2000px;
  }
</style>

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>

        <!-- Icon Cards-->
        <div class="card-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Nama Homestay</th>
                <th>Alamat</th>
                <th>Deskripsi Lokasi</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($show as $show)
              <tr>
                <td>{{$show->nama_homestay}}</td>
                <td>{{$show->alamat}}</td>
                <td>{{$show->area_terdekat}}</td>
                <td id="action">
                <a href="/hapus/{{$show->id}}"><button type="button" name="Hapus" class="btn btn-danger" id="edit">Hapus</button></a>
                <a href="/DetailHomestay/{{$show->id}}" method="GET"><button type="button" name="Hapus" class="btn btn-success" id="edit">Lihat</button></a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->

  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->

</div>
@endsection
