@extends('layouts.app')

@section('content')
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>
        @foreach($homestay as $homestay)
        <img src="/images/{{$homestay->gambar}}" style="height : 200px; width : 200px;" class="rounded float-left" alt="...">
        <img src="/images/{{$homestay->gambar}}" style="height : 200px; width : 200px; margin-left : 50px;" alt="">
        <br><br>
        <table>
            <tr>
                <th>Nama Homestay</th>
                <th>:</th>
                <th>{{$homestay->nama_homestay}}</th>
            </tr>
            <tr>
                <th>Alamat Lengkap</th>
                <th>:</th>
                <th>{{$homestay->alamat}}</th>
            </tr>
            <tr>
                <th>Jumlah Kamar</th>
                <th>:</th>
                <th>{{$homestay->jumlah_kamar}}</th>
            </tr>
            <tr>
                <th>Haraga</th>
                <th>:</th>
                <th>{{$homestay->harga}}</th>
            </tr>
            <tr>
                <th>Area Terdekat Dengan Homestay</th>
                <th>:</th>
                <th>{{$homestay->area_terdekat}}</th>
            </tr>
            <tr>
                <th>Fasilitas Yang Tersedia</th>
                <th>:</th>
                <th>{{$homestay->fasilitas}}</th>
            </tr>
        </table>
        @endforeach       
        <br><br><br>
        <h4>Kamar Pada Homestay : </h4>
            <div class="row">
            @foreach($detail as $kamar)
                <?php
                    $harga = number_format($kamar->harga_kamar,0,",",".");
                ?>
                <div class="col-6 col-md-4">
                    <div class="card" style="width:250px">
                        <img class="card-img-top" src="/images/{{$kamar->gambar_kamar}}" alt="Card image" style="width:100%">
                        <div class="card-body">
                            <h4 class="card-title">Nomor Kamar: {{$kamar->no_kamar}}</h4>
                            <h4 class="card-title">Jumlah Orang : {{$kamar->jumlah_orang}}</h4>
                        <h4 class="card-title" style="color : blue">Harga : Rp.{{$harga}}</h4>
                        <h4 class="card-title" style="color : red">status : {{$kamar->status}}</h4>
                        </div>
                    </div>     
                </div>
            @endforeach    
            </div>
@endsection