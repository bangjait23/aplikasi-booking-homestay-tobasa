@extends('layouts.app')

@section('content')
<style media="screen">
#navside{
  background-color:  #32EF7E;
}
input[type=submit]:hover, select{
  background-color: #0000FF;
}

</style>
<script>
    function onlyNumber(){
        var no = document.forms["kamar"]["nomor"].value;
        var orang = document.forms["kamar"]["orang"].value;
        var harga = document.forms["kamar"]["harga"].value;
        var number = /^[0-9]+$/;

        if(!no.match(number)){
            alert("Form Nomor Kamar Harus Berisi Angka!");
            return false;
        }
        if(!orang.match(number)){
            alert("Form Jumlah Orang Harus Berisi Angka!");
            return false;
        }
        if(!harga.match(number)){
            alert("Form Harga Kamar Harus Berisi Angka!");
            return false;
        }
    }
</script>
            
            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
              </li>
              <li class="breadcrumb-item active">Overview</li>
            </ol>
            <div class="card-body">
                <form method="POST" name="kamar" action="/tambahKamar/{{$owner}}/{{$nama_homestay}}/{{$id_homestay}}" enctype="multipart/form-data" onsubmit="return onlyNumber()">
                    @csrf

                    <div class="form-group row">
                        <label for="nama_homestay" class="col-md-4 col-form-label text-md-right">{{ __('Nomor Kamar') }}</label>

                        <div class="col-md-6">
                            <input id="nomor_kamar" type="text" class="form-control" name="nomor" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama_homestay" class="col-md-4 col-form-label text-md-right">Jumlah Orang</label>

                        <div class="col-md-6">
                            <input id="nomor_kamar" type="text" class="form-control" name="orang"  required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="harga" class="col-md-4 col-form-label text-md-right">Harga</label>

                        <div class="col-md-6">
                            <input id="email" type="text" class="form-control" name="harga" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="alamat" class="col-md-4 col-form-label text-md-right">Photo kamar</label>

                        <div class="col-md-6">
                            <input id="email" type="file" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="gambar" value="{{ old('email') }}">
                        </div>
                    </div>
                    <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary" style="width : 150px;">
                                        Tambah Kamar
                                    </button>
                                </div>
                </form>
            </div>

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="login.html">Logout</a>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript-->

</div>

@endsection
