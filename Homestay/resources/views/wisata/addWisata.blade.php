@extends('layouts.app')

@section('content')
<style media="screen">
#navside{
  background-color:  #32EF7E;
}

</style>


            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
              </li>
              <li class="breadcrumb-item active">Overview</li>
            </ol>
            <div class="card-body">
                <form method="POST" action="/tambahWisata" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama Tempat Wisata') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" name="nama_wisata" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Alamat Lengkap Tempat Wisata') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="text" name="alamat" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                        </div>
                    </div>

                    <div class="form-group row">
                      <label for="exampleFormControlTextarea1">Deskripsi Tempat Wisata</label>
                      <textarea class="form-control" rows="7" placeholder="Deskripsi Daerah Wisata" name="deskripsi"></textarea>
                    </div>
                    <div class="form-group row">
                      <label for="exampleFormControlTextarea1">Gambar Tempat Wisata</label>
                      <input class="form-control" type="file" name="gambar">
                    </div>
                    <button class="btn btn-secondary" style="width : 100px;">Tambah</button>
                </form>
            </div>

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="login.html">Logout</a>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript-->

</div>

@endsection
