@extends('layouts.app')

@section('content')

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Overview</li>
</ol>

        <!-- Icon Cards-->
<div class="card-body">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Nama Wisata</th>
                <th>Alamat</th>
                <th>Deskripsi Lokasi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($wisata as $wisata) 
              <tr>
                <td>{{$wisata->nama_wisata}}</td>
                <td>{{$wisata->alamat}}</td>
                <td>{{$wisata->deskripsi}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      <!-- /.containe

@endsection