<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- Scripts -->
    <script src="{{asset('/js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{asset('css/app.css') }}" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="{{asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">
    <style media="screen">
        #navbar{
          background-color:  #6610f2;
          height: 100px;
        }
        #navbar li a{
          color: white;
        }
        #link a:hover{
          color: white;
          text-align: center;
        }
        #footer{
          background-color:  #6610f2;
          height: 100px;
        }
    </style>
</head>
<body >
    <div id="app">
        <nav class="navbar navbar-expand navbar-dark  static-top" id="navbar">
            <div class="container">
                <a class="navbar-brand" >
                    {{ config('Bagasta', 'Bagasta') }}
                </a>
                <ul class="navbar-nav ml-auto"id="link">
                  <li class="nav-item">
                      <a href="/Homestay" class="nav-link">Homestay</a>
                  </li>
                </ul>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto"id="link">
                        <!-- Authentication Links -->
                        @guest
                        <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                          <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="button">
                                <i class="fas fa-search"></i>
                              </button>
                            </div>
                          </div>
                        </form>

                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif
                            </li>
                        @else
                        <li class="nav-item">
                          <li class="nav-item dropdown no-arrow mx-1">
                            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-bell fa-fw"></i>
                              <span class="badge badge-danger">9+</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
                              <a class="dropdown-item" href="#">Action</a>
                              <a class="dropdown-item" href="#">Another action</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                          </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fas fa-user-circle fa-fw"></i>{{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-item" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
      <div id="wrapper" id="content">
      @if(Auth::user()->status == 'Admin')
        <ul class="sidebar navbar-nav" id="navside">
          <li class="nav-item dropdown">
            <a class="nav-link" href="/home" id="pagesDropdown" role="button" >
              <i class="fas fa-fw fa-home"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link" href="/Homestay" id="pagesDropdown" role="button" >
              <i class="fas fa-fw fa-home"></i>
              <span>Data Homestay</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/dataWisata">
              <i class="fas fa-fw fa-table"></i>
              <span>Data Wisata</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/RegAdmin ">
              <i class="fas fa-fw fa-table"></i>
              <span>Register Owner</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/AddWisata">
                <i class="fas fa-fw fa-table"></i>
                <span>Tambah Daerah Wisata</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/addDataAdmin">
                <i class="fas fa-fw fa-table"></i>
                <span>Tambah Data Admin</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/getAdmin" method="GET">
                <i class="fas fa-fw fa-table"></i>
                <span>Data Admin</span></a>
            </li>
        </ul>
      @else
      <ul class="sidebar navbar-nav" id="navside">
          <li class="nav-item dropdown">
            <a class="nav-link" href="/home" id="pagesDropdown" role="button" >
              <i class="fas fa-fw fa-home"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link" href="/viewHomestay/{{Auth::user()->email}}" id="pagesDropdown" role="button" >
              <i class="fas fa-fw fa-home"></i>
              <span>Data Homestay</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/dataPemesan/{{Auth::user()->email}}">
              <i class="fas fa-fw fa-chart-area"></i>
              <span>Data Pemesanan Homestay</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/dataPemesanKamar/{{Auth::user()->email}}">
              <i class="fas fa-fw fa-chart-area"></i>
              <span>Data Pemesanan Kamar</span></a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="/AddHomestay/{{Auth::user()->email}}">
                <i class="fas fa-fw fa-table"></i>
                <span>Tambah Homestay</span></a>
            </li>
      </ul>
            @endif 
      <div class="container-fluid" >
        <main class="py-4">
            @yield('content')
        </main>
      </div>
      </div>


        <footer>
          <div class="jumbotron text-center" style="margin-bottom:0" id="footer">
              <div class="container my-auto">
                <div class="copyright text-center my-auto">
                  <span>Copyright © Your Website 2019</span>
                </div>
              </div>
            </div>
        </footer>
    </div>
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Page level plugin JavaScript-->
    <script src="{{asset('vendor/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('js/sb-admin.min.js')}}"></script>

    <!-- Demo scripts for this page-->
    <script src="{{asset('js/demo/datatables-demo.js')}}"></script>
    <script src="{{asset('js/demo/chart-area-demo.js')}}"></script>
</body>
</html>
