@extends('layouts.app')

@section('content')

            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
              </li>
              <li class="breadcrumb-item active">Overview</li>
            </ol>
            <h1>Detail Data Pemesanan</h1>
            @foreach($kamar as $kamar)
            <table>
                <thead>
                  <tr>
                    <th><h4>Nama Pemesan</h4></th>
                    <th><h4>:</h4></th>
                    <th><h4>{{$kamar->nama_pemesan}}</h4></th>
                  </tr>
                  <tr>
                    <th><h4>Tanggal Check In</h4></th>
                    <th><h4>:</h4></th>
                    <th><h4>{{$kamar->tanggal_masuk}}</h4></th>
                  </tr>
                  <tr>
                    <th><h4>Tanggal Check Out</h4></th>
                    <th><h4>:</h4></th>
                    <th><h4>{{$kamar->tanggal_keluar}}</h4></th>
                  </tr>
                  <tr>
                    <th><h4>Nama Homestay</h4></th>
                    <th><h4>:</h4></th>
                    <th><h4>{{$kamar->total_pembayaran}}</h4></th>
                  </tr>
                  <tr>
                    <th><h4>Status pemesanan</h4></th>
                    <th><h4>:</h4></th>
                    <th><h4>{{$kamar->status}}</h4></th>
                  </tr>
                </thead>
              </table><br>
              <?php 
                $today = date("Y-n-j");
                // echo $today;
              ?>
              @if($kamar->status =="Pesan")
                @if($kamar->tanggal_masuk == $today)
              <a href="/acceptPesanKamar/{{$kamar->id}}/{{$kamar->id_kamar}}" method="GET"><button class="btn btn-success" style="margin-left : 300px; width : 100px;">Check In</button></a>
              @else
              <p>Tunggu konfirmasi</p>
              @endif 
              @else
                @if($kamar->tanggal_keluar == $today)
                  <a href="/keluarPesanKamar/{{$kamar->id}}/{{$kamar->id_kamar}}"><button class="btn btn-secondary" style="margin-left : 300px; width : 100px;">Check Out</button></a>
                @else
                  <p>Pengunjung Belum Bisa keluar</p>
                @endif  
              @endif
              @endforeach                 
@endsection