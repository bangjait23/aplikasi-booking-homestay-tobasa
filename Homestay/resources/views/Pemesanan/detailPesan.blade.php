@extends('layouts.app')

@section('content')

            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
              </li>
              <li class="breadcrumb-item active">Overview</li>
            </ol>
            <h1>Detail Data Pemesanan</h1>
            @foreach($pesan as $pesan)
            <table>
                <thead>
                  <tr>
                    <th><h4>Nama Pemesan</h4></th>
                    <th><h4>:</h4></th>
                    <th><h4>{{$pesan->nama_pemesan}}</h4></th>
                  </tr>
                  <tr>
                    <th><h4>Tanggal Check In</h4></th>
                    <th><h4>:</h4></th>
                    <th><h4>{{$pesan->tanggal_checkin}}</h4></th>
                  </tr>
                  <tr>
                    <th><h4>Tanggal Check Out</h4></th>
                    <th><h4>:</h4></th>
                    <th><h4>{{$pesan->tanggal_checkout}}</h4></th>
                  </tr>
                  <tr>
                    <th><h4>Nama Homestay</h4></th>
                    <th><h4>:</h4></th>
                    <th><h4>{{$pesan->nama_homestay}}</h4></th>
                  </tr>
                  <tr>
                    <th><h4>Status pemesanan</h4></th>
                    <th><h4>:</h4></th>
                    <th><h4>{{$pesan->status}}</h4></th>
                  </tr>
                </thead>
              </table><br>
              @if($pesan->status =="pesan")
              <a href="/accept/{{$pesan->id}}/{{$pesan->id_homestay}}" method="GET"><button class="btn btn-success" style="margin-left : 300px; width : 100px;">Check In</button></a> 
              @else
              <a href="/keluar/{{$pesan->id}}/{{$pesan->id_homestay}}"><button class="btn btn-secondary" style="margin-left : 300px; width : 100px;">Check Out</button></a>
              @endif
              @endforeach                 
@endsection