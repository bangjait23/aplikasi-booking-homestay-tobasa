<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class homestay extends Model
{
    protected $table="homestays";
    protected $fillable = ['nama_homestay', 'alamat', 'area_terdekat'];
}
