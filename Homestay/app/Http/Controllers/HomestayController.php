<?php

namespace App\Http\Controllers;
use App\homestay;
use App\kamar;
use App\pesanKamar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\KritikSaran;
class HomestayController extends Controller
{
    public function home(){
      $show = DB::select('select * from homestays');
      return view('/Homestay/homestay',compact('show'));
    }
    public function GoToForm($email){
      return view('/Homestay/AddHomestay', compact('email'));
    }
    public function tambahHomestay(Request $req, $email){
      $homestay = new homestay();

      $homestay->nama_homestay=$req->nama_homestay;
      $homestay->alamat=$req->alamat;
      $homestay->harga = $req->harga;
      $homestay->jumlah_kamar = $req->jumlah_kamar;
      $homestay->area_terdekat=$req->area_terdekat;
      $homestay->fasilitas = $req->fasilitas;
      $homestay->owner = $email;
      $homestay->kontak = $req->kontak;
      $homestay->no_rekening = $req->no_rekening;
      $homestay->status_homestay="Kosong";
       
      
      $file = $req->file('gambar');
      $filename = $file->getClientOriginalName();
      $destination = $req->file('gambar')->store('');
      $req->file('gambar')->move('images',$destination);
      $homestay->gambar = $destination;

      //Gambar Kamar
      $file = $req->file('gambar_toilet');
      $filename = $file->getClientOriginalName();
      $destination = $req->file('gambar_toilet')->store('');
      $req->file('gambar_toilet')->move('images',$destination);
      $homestay->gambar_toilet = $destination;
      $homestay->save();

      return redirect('/Homestay');
    }
    public function edit($id){
      $edit = DB::table('homestays')->where('id',$id)->get();
      return view('Homestay.EditHomestay',compact('edit'));
    }
    public function viewHomestay($id){
        $homestay = DB::select("select * from homestays where id = '$id'");
        return view('Homestay.homestayDetail',compact('homestay'));
    }
    public function HomestayOwner($email){
      $homestay = DB::select("select * from homestays where owner like '$email'");
      $kamar = DB::select("select * from kamars where owner like '$email'");
      return view('Homestay.homestayOwner',compact('homestay','kamar'));
    }
    public function formAddKamar($owner, $nama_homestay, $id_homestay){
      return view('Homestay.formAddKamar', compact('owner','nama_homestay','id_homestay'));
    }
    public function tambahKamar(Request $request, $owner, $homestay, $id_homestay){
      $kamar = new kamar;
      $kamar->id_homestay = $id_homestay;
      $kamar->no_kamar = $request->nomor;
      $kamar->jumlah_orang = $request->orang;
      $kamar->nama_homestay = $homestay;
      $kamar->harga_kamar = $request->harga;
      $kamar->owner = $owner;
      $kamar->status = "Available";

      $file = $request->file('gambar');
      $filename = $file->getClientOriginalName();
      $destination = $request->file('gambar')->store('');
      $request->file('gambar')->move('images',$destination);
      $kamar->gambar_kamar = $destination;
      $kamar->save();
      Session::flash('message', 'Input data sukses!');
      return back();     
    }
    public function detailpesanKamar($id){
        $kamar = DB::select("select * from pesan_kamars where id = '$id'");
        return view('User.detailPesanKamar', compact('kamar'));
    }
    public function kritik($homestay){
      $kritik = DB::select("select * from kritik_sarans where nama_homestay like '$homestay'");

      return view('Homestay.kritikDansaran', compact('kritik'));
    }
    public function deleteKritik($id){
      $delete = DB::delete("delete from kritik_sarans where id = $id");
      return back();
    }
    public function hapusHomestay($id){
      $deleteHomestay  = DB::delete("delete from homestays where id = $id");
      return back();
    }
    public function detailHomestay($id){
      $homestay = DB::select("select * from homestays where id = $id");
      $detail = DB::select("select * from kamars where id_homestay = $id");
      return view('Homestay.homestayDetail', compact('detail','homestay'));
    }
}
