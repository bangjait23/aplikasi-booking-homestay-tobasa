<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\pemensanan;
use Illuminate\Support\Facades\Hash;
use App\user;
use App\pesanKamar;
use App\kamar;
use App\admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
class HomeController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function addAdmin(Request $request){
        $user = new user();
    
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->kontak = $request->kontak;
        $user->status = "Owner";

        $user->save();
        
        return redirect('/home');
    }
    public function RegAdmin(){
      return view('/User/RegisterOwner');
    }
    public function ShowData($email){
      $pemesanan = DB::select("select * from pemensanans where owner like '$email'");   
      return view('User.dataPemesan',compact('pemesanan'));
    }
    public function ShowDataPesanKamar($email){
        $pemesanan = DB::select("select * from pesan_kamars where owner like '$email'");   
        return view('User.dataPemesanKamar',compact('pemesanan'));
      }
    public function detail($id){
        $pesan = DB::select('select * from pemensanans where id  = '.$id);
        return view('Pemesanan.detailPesan',compact('pesan'));
    }
    public function accept($id, $id_homestay){
        $accept = DB::update("update pemensanans set status = 'Check In'
        where id = ".$id);
        $berisi = DB::update("update homestays set status_homestay = 'Berisi' where id = '$id_homestay'");
        return back();
      }
      public function acceptPesanKamar($id, $id_kamar){
        $accept = DB::update("update pesan_kamars set status = 'Check In'
        where id = ".$id);
        $berisi = DB::update("update kamars set status = 'Not Available' where id = '$id_kamar'");
        return back();
      }
      public function keluarPesanKamar($id, $id_kamar){
        $accept = DB::update("update pesan_kamars set status = 'Check out'
        where id = ".$id);
        $berisi = DB::update("update kamars set status = 'Available' where id = '$id_kamar'");
        return back();
      }
      public function keluar($id, $id_homestay){
        $accept = DB::update("update pemensanans set status = 'Check Out'
        where id = ".$id);
        $berisi = DB::update("update homestays set status_homestay = 'Kosong' where id = '$id_homestay'");
        return back();
      }
    
    public function tambahAdmin(){
        return view('auth.addAdmin');
    }
    public function dataAdmin(Request $req){
        $admin = new admin();

        $admin->nama = $req->nama;
        $admin->email = $req->email;
        $admin->kontak=$req->kontak;
        $admin->alamat = $req->kontak;
        $admin->jurusan = $req->jurusan;
        
        $file = $req->file('gambar');
        $filename = $file->getClientOriginalName();
        $destination = $req->file('gambar')->store('');
        $req->file('gambar')->move('images',$destination);
        $admin->gambar = $destination;
        $admin->save();
        return redirect('/getAdmin');
    }
    public function getAdmin(){
        $admin = admin::all();
        return view('auth.dataAdmin',compact('admin'));
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {   
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

      /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $data)
    {   
        $user = new User([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'status'=>'Owner',
        ]);
        $user->save();
            return view('/home');
    }
   
}
