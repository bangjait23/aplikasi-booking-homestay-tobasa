<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\homestay;
use App\pemensanan;
use App\wisata;
use App\pesanKamar;
use App\admin;
use Illuminate\Support\Facades\DB;

class APIController extends Controller
{
    public function index(){
        $bogas = DB::select("select * from homestays");
        return $bogas;
    }
    public function home($nama_homestay){
        $show =  $homestay = DB::select("select * from homestays where nama_homestay like '$nama_homestay'");
        return $show;
      }
      public function detailVIew($id){
          $show = homestay::find($id);
            return $show;
      }
      public function tambahPesanan(Request $request){
          $pesan = new pemensanan();

          $pesan->nama_pemesan = $request->nama_pemesan;
          $pesan->tanggal_checkin = $request->tanggal_checkout;
          $pesan->tanggal_checkout = $request->tanggal_checkout;
          $pesan->nama_homestay = $request->nama_homestay;
          $pesan->id_customer = $request->id_customer;

          return $pesan;
      }
      public function pemesanan($username){
          $pemesanan = DB::select("select * from pemensanans where username like '%$username%'");
          return $pemesanan;
      }
      public function dataWisata(){
        $wisata = wisata::all();
        return $wisata;
      }
      public function detailWisata($nama_wisata){
          $detail = DB::select("select * from wisatas where nama_wisata like '%$nama_wisata%'");
          return $detail;
      }
      public function getAdmin(){
        $admin = admin::all();
        return $admin;
    }
    public function viewHomestay($nama_homestay){
        $homestay = DB::select("select * from homestays inner join kamars on homestays.id = kamars.id_homestay
        where homestays.nama_homestay like '$nama_homestay'");
        return $homestay;
    }
    public function viewKamarHomestay($id_homestay){
        $kamar = DB::select("select *from kamars where id_homestay = '$id_homestay'");
        return $kamar;
    }
    public function pemesananKamar($username){
        $pesanan = DB::select("select * from pesan_kamars where username_pemesan like '$username'");
        return $pesanan;
    }
}
