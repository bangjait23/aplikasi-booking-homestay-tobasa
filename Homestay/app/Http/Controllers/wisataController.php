<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\wisata;
use Illuminate\Support\Facades\DB;
class wisataController extends Controller
{
    public function AddWisata(){
      return view('wisata.addWisata');
    }
    public function tambahWisata(Request $request){
      $wisata = new wisata();
      $wisata->nama_wisata = $request->nama_wisata;
      $wisata->alamat = $request->alamat;
      $wisata->deskripsi = $request->deskripsi;

      $file = $request->file('gambar');
      $filename = $file->getClientOriginalName();
      $destination = $request->file('gambar')->store('');
      $request->file('gambar')->move('images',$destination);
      $wisata->gambar = $destination;
      $wisata->save();

      return redirect('/dataWisata');
    }
    public function dataWisata(){
      $wisata = wisata::all();
      return view('wisata.dataWisata', compact('wisata'));
    }
}
