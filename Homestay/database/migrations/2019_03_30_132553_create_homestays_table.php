<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomestaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homestays', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_homestay');
            $table->string('alamat');
            $table->integer('jumlah_kamar');
            $table->integer('harga');
            $table->string('area_terdekat');
            $table->string('gambar');
            $table->string('fasilitas');
            $table->string('gambar_toilet');
            $table->string('status_homestay');
            $table->bigInteger('Kontak');
            $table->integer('no_rekening');
            $table->string('owner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homestays');
    }
}
