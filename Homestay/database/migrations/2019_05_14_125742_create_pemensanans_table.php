<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemensanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemensanans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_homestay');
            $table->string('nama_pemesan');
            $table->string('tanggal_checkin');
            $table->string('tanggal_checkout');
            $table->integer('total');
            $table->string('nama_homestay');
            $table->string('username');
            $table->string('status');
            $table->string('owner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemensanans');
    }
}
