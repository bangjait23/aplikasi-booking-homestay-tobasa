<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesanKamarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesan_kamars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_pemesan');
            $table->string('nama_homestay');
            $table->integer('id_homestay');
            $table->integer('no_kamar');
            $table->integer('id_kamar');
            $table->integer("total_pembayaran");
            $table->string('status');
            $table->string('tanggal_masuk');
            $table->string('tanggal_keluar');
            $table->string('username_pemesan');
            $table->string('owner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesan_kamars');
    }
}
