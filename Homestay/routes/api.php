<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



//routing untuk homestay
Route::get('/bogasta','APIController@index');
Route::get('/Homestay/{nama_homestay}','APIController@home');
Route::get('/pemesanan/{username}','APIController@pemesanan');
Route::get('/dataWisata','APIController@dataWisata');
Route::get('/detailWisata/{nama_wisata}','APIController@detailWisata');
Route::get('/getAdmin','APIController@getAdmin');
Route::get('/viewHomestay/{nama_homestay}','APIController@viewHomestay');
Route::get('/viewKamarHomestay/{id_homestay}','APIController@viewKamarHomestay');
Route::get('/pemesananKamar/{username}','APIController@pemesananKamar');