<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

//Routing untuk autentikasi
Route::get('/home', 'HomeController@index')->name('home');

//Routing untuk admin
Route::get('/RegAdmin','HomeController@RegAdmin');
Route::post('/addAdmin','HomeController@addAdmin');

//Routing untuk pemesanan
Route::get('/dataPemesan/{email}','HomeController@ShowData');
Route::get('/dataPemesanKamar/{email}','HomeController@ShowDataPesanKamar');
Route::get('/detailPesanKamar/{id}','HomestayController@detailPesanKamar');
Route::get('/detailPesan/{id}','HomeController@detail');
Route::get('/accept/{id}/{id_homestay}','HomeController@accept');
Route::get('/keluar/{id}/{id_homestay}','HomeController@keluar');
Route::get('/acceptPesanKamar/{id}/{id_kamar}','HomeController@acceptPesanKamar');
Route::get('/keluarPesanKamar/{id}/{id_kamar}','HomeController@keluarPesanKamar');
Route::get('/kritik/{homestay}','HomestayController@kritik');
Route::get('/deleteKritik/{id}','HomestayController@deleteKritik'); 

//Routing untuk wisata
Route::get('/AddWisata','wisataController@AddWisata');

//routing untuk homestay
Route::get('/Homestay','HomestayController@home');
Route::get('/AddHomestay/{email}','HomestayController@GoToForm');
Route::post('/tambahHomestay/{email}','HomestayController@tambahHomestay');
Route::get('/edit/{id}','HomestayController@edit');
Route::get('/viewHomestay/{email}','HomestayController@HomestayOwner');
Route::get('/formAddKamar/{owner}/{nama_homestay}/{id_homestay}','HomestayController@formAddKamar');
Route::post('/tambahKamar/{owner}/{nama_homestay}/{id_homestay}','HomestayController@tambahKamar');
Route::get('/hapus/{id}','HomestayController@hapusHomestay');
Route::get('/DetailHomestay/{id}','HomestayController@DetailHomestay');

//wisata
Route::get('/dataWisata','WisataController@dataWisata');
Route::post('/tambahWisata','WisataController@tambahWisata');
Route::get('/addDataAdmin','HomeController@tambahAdmin');
Route::post('/dataAdmin','HomeController@dataAdmin');
Route::get('/getAdmin','HomeController@getAdmin');
Route::get('/viewHomestay/{id}','HomestayController@viewHomestay');
Route::post('/RegisterOwner','HomeController@create');
