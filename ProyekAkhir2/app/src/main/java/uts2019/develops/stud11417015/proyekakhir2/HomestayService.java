package uts2019.develops.stud11417015.proyekakhir2;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by test on 5/9/2019.
 */

public interface HomestayService {
    @GET("bogasta")
    Call<List<HomestayModel>> getHome();


    @GET("Homestay/{nama_homestay}")
    Call<List<HomestayModel>> getHomestay(@Path("nama_homestay") String nama_homestay);

    @GET("http://192.168.43.122/Homestay/public/api/pemesanan/{username}")
    Call<List<PemesananModel>> getPemesanan(@Path("username") String username);
    //Get Data By Id
//    @GET("detailView/{id}")
//    Call<List<HomestayModel>> getDetail(@Path("id") int id);

    @GET("dataWisata")
    Call<List<wisataModel>> getWisata();

    @GET("http://192.168.43.122/Homestay/public/api/detailWisata/{nama_homestay}")
    Call<List<wisataModel>>getDetailWisata(@Path("nama_homestay") String nama_homestay);

    @GET("getAdmin")
    Call<List<AboutModel>> getAdmin();

    @GET("viewKamarHomestay/{id_homestay}")
    Call<List<KamarModel>> viewKamarHomestay(@Path("id_homestay") String id_homestay);

    @GET("pemesananKamar/{username}")
    Call<List<pesanKamarModel>> viewPesanKamar(@Path("username") String username);
}
