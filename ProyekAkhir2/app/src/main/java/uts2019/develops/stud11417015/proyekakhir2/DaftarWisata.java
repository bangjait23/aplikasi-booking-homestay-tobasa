package uts2019.develops.stud11417015.proyekakhir2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DaftarWisata extends Fragment {
    GridView gridView;
    private int[] images = new int[]{
            R.drawable.carousel1, R.drawable.siboruon, R.drawable.pasir_putih, R.drawable.lumban_bulbul
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_daftar_wisata, container, false);
        CarouselView carouselView = (CarouselView) view.findViewById(R.id.carousel);
        carouselView.setPageCount(images.length);
        carouselView.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                imageView.setImageResource(images[position]);
            }
        });
        HomestayService homestayService = APIClient.getRetrofit().create(HomestayService.class);
        final Call<List<wisataModel>> wisataCall = homestayService.getWisata();
        wisataCall.enqueue(new Callback<List<wisataModel>>() {
            @Override
            public void onResponse(Call<List<wisataModel>> call, Response<List<wisataModel>> response) {
                if(response.isSuccessful()){
                    tampilkanWisata(response.body());
                }else {
                    Log.e("Error Message", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<wisataModel>> call, Throwable t) {
                Log.e("Message", t.getMessage());
            }
        });

        return view;
    }
    private void tampilkanWisata(List<wisataModel> wisata){
        WisataAdapter wisataAdapter = new WisataAdapter(getContext(), R.layout.wisata_item, wisata);
        gridView = (GridView) getActivity().findViewById(R.id.gridView1);
        gridView.setAdapter(wisataAdapter);
        wisataAdapter.notifyDataSetChanged();
    }

}
