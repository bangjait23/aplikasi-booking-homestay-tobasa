package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomepageContent extends AppCompatActivity {
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage_content);
        HomestayService homestayService = APIClient.getRetrofit().create(HomestayService.class);
        final Call<List<HomestayModel>> homestayCall = homestayService.getHome();

        homestayCall.enqueue(new Callback<List<HomestayModel>>() {
            @Override
            public void onResponse(Call<List<HomestayModel>> call, Response<List<HomestayModel>> response) {
                if (response.isSuccessful()){
                    tampilkan(response.body());
                }else {
                    Log.e("Error Message", response.message());
                }
            }
            @Override
            public void onFailure(Call<List<HomestayModel>> call, Throwable t) {
                Log.e("Message", t.getMessage());
            }
        });

    }
    private void loadhomestay(String homestay){
        HomestayService homestayService = APIClient.getRetrofit().create(HomestayService.class);
        final Call<List<HomestayModel>> homestayCall = homestayService.getHomestay(homestay);

        homestayCall.enqueue(new Callback<List<HomestayModel>>() {
            @Override
            public void onResponse(Call<List<HomestayModel>> call, Response<List<HomestayModel>> response) {
                if (response.isSuccessful()){
                    tampilkan(response.body());
                }else {
                    Log.e("Error Message", response.message());
                }
            }
            @Override
            public void onFailure(Call<List<HomestayModel>> call, Throwable t) {
                Log.e("Message", t.getMessage());
            }
        });
    }
    private void tampilkan(List<HomestayModel> home){
        HomestayPageAdapter arrayHomestay = new HomestayPageAdapter(getApplicationContext(), R.layout.homestay_items, home);
        listView = (ListView) findViewById(R.id.list_buku);
        listView.setAdapter(arrayHomestay);
        arrayHomestay.notifyDataSetChanged();
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem item = menu.findItem(R.id.menuSearch);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setQueryHint("Cari Homestay....");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                loadhomestay(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

        });
        loadhomestay("");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.login) {
            startActivity(new Intent(getApplicationContext(), Login.class));
        }
        else if(id == R.id.register){
            startActivity(new Intent(getApplicationContext(), Register.class));
        }

        return super.onOptionsItemSelected(item);
    }
}
