package uts2019.develops.stud11417015.proyekakhir2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by test on 6/9/2019.
 */

public class pesanKamarModel  {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("nama_pemesan")
    @Expose
    private String namaPemesan;
    @SerializedName("id_homestay")
    @Expose
    private String id_homestay;
    @SerializedName("nama_homestay")
    @Expose
    private String nama_homestay;
    @SerializedName("no_kamar")
    @Expose
    private String no_kamar;
    @SerializedName("id_kamar")
    @Expose
    private String id_kamar;
    @SerializedName("total_pembayaran")
    @Expose
    private String total;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("tanggal_masuk")
    @Expose
    private String tanggalMasuk;
    @SerializedName("tanggal_keluar")
    @Expose
    private String tangalKeluar;

    public pesanKamarModel(String id, String namaPemesan, String id_homestay, String nama_homestay, String no_kamar, String id_kamar, String total, String status, String tanggalMasuk, String tangalKeluar) {
        this.id = id;
        this.namaPemesan = namaPemesan;
        this.id_homestay = id_homestay;
        this.nama_homestay = nama_homestay;
        this.no_kamar = no_kamar;
        this.id_kamar = id_kamar;
        this.total = total;
        this.status = status;
        this.tanggalMasuk = tanggalMasuk;
        this.tangalKeluar = tangalKeluar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamaPemesan() {
        return namaPemesan;
    }

    public void setNamaPemesan(String namaPemesan) {
        this.namaPemesan = namaPemesan;
    }

    public String getId_homestay() {
        return id_homestay;
    }

    public void setId_homestay(String id_homestay) {
        this.id_homestay = id_homestay;
    }

    public String getNama_homestay() {
        return nama_homestay;
    }

    public void setNama_homestay(String nama_homestay) {
        this.nama_homestay = nama_homestay;
    }

    public String getNo_kamar() {
        return no_kamar;
    }

    public void setNo_kamar(String no_kamar) {
        this.no_kamar = no_kamar;
    }

    public String getId_kamar() {
        return id_kamar;
    }

    public void setId_kamar(String id_kamar) {
        this.id_kamar = id_kamar;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTanggalMasuk() {
        return tanggalMasuk;
    }

    public void setTanggalMasuk(String tanggalMasuk) {
        this.tanggalMasuk = tanggalMasuk;
    }

    public String getTangalKeluar() {
        return tangalKeluar;
    }

    public void setTangalKeluar(String tangalKeluar) {
        this.tangalKeluar = tangalKeluar;
    }
}
