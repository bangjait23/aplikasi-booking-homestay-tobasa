package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    EditText username;
    EditText password;
    Button btnLog, registers;
    DatabaseHelper db;
    public static String usernames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        db = new DatabaseHelper(this);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        btnLog = (Button) findViewById(R.id.login);
        registers = (Button) findViewById(R.id.register);
        btnLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uname = username.getText().toString();
                String pwd = password.getText().toString();
                if (db.login(uname, pwd)) {
                    usernames = uname;
                    startActivity(new Intent(Login.this, MainActivity.class));
                    finish();
                    Toast.makeText(getApplicationContext(), "Login Berhasil", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Akun tidak terdaftar silahkan coba lagi", Toast.LENGTH_SHORT).show();
                }

            }
        });
        registers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Register.class));
            }
        });
    }

    public static String getUsername() {
        return usernames;
    }
}
