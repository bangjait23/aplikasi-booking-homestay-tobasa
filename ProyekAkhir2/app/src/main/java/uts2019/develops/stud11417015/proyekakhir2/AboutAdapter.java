package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by test on 5/23/2019.
 */

public class AboutAdapter extends ArrayAdapter<AboutModel>{
    private Context context;
    public AboutAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<AboutModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.about_item, parent, false
            );
        }
        Picasso.Builder picassos = new Picasso.Builder(context);
        final AboutModel aboutModel = getItem(position);

        TextView nama = (TextView) convertView.findViewById(R.id.nama);
        nama.setText(aboutModel.getNama());

        ImageView image = (ImageView) convertView.findViewById(R.id.image);
        Picasso picasso = picassos.build();
        picasso.load("http://192.168.43.122/Homestay/public/images/"+aboutModel.getGambar()).into(image);

        return convertView;
    }
}
