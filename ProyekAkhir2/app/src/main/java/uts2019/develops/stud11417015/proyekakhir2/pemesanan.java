package uts2019.develops.stud11417015.proyekakhir2;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class pemesanan extends AppCompatActivity {
    public static final String URL = "http://192.168.43.122/PesanHomestay/";
    DatePickerDialog datePickerDialog;
    int mday, mMonth, myear;
    EditText pemesan, checkin, checkOut, jmulah_pengunjung;
    TextView nama_homestay, kontak, rekening;
    Button booking;
    private ProgressDialog progress;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemesanan);
        Intent intent = getIntent();

        final String owner = intent.getExtras().getString("owner");
        final String homestay = intent.getExtras().getString("nama_homestay");
        final String harga = intent.getExtras().getString("harga");
        final int id_homestay = Integer.parseInt(intent.getExtras().getString("id_homestay"));
        final String status_homestay = intent.getExtras().getString("status");
        pemesan = (EditText) findViewById(R.id.nama_pemesan);
        kontak = (TextView) findViewById(R.id.kontak);
        kontak.setText(KamarActivity.getKontak());

        rekening = (TextView) findViewById(R.id.rekening);
        rekening.setText(KamarActivity.getRekening());
        checkin = (EditText) findViewById(R.id.tanggal_checkin);
        checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(pemesanan.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                checkin.setText(year + "-"
                                        + (monthOfYear + 1) + "-" + dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        checkOut = (EditText) findViewById(R.id.tanggal_checkout);
        checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear1 = c.get(Calendar.YEAR); // current year
                int mMonth1 = c.get(Calendar.MONTH); // current month
                int mDay1 = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(pemesanan.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year1,
                                                  int monthOfYear1, int dayOfMonth1) {
                                // set day of month , month and year value in the edit text
                                checkOut.setText(year1 + "-"
                                        + (monthOfYear1 + 1) + "-" + dayOfMonth1);

                            }
                        }, mYear1, mMonth, mDay1);
                datePickerDialog.show();
            }
        });
        jmulah_pengunjung = (EditText) findViewById(R.id.jumlah_orang);
        nama_homestay = (TextView) findViewById(R.id.nama_homestay);
        nama_homestay.setText(homestay);
        booking = (Button) findViewById(R.id.booking);
        booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama_pemesan = pemesan.getText().toString();
                String tanggal_checkin = checkin.getText().toString();
                String tanggal_checkout = checkOut.getText().toString();

                String homestays = homestay;
                String username = Login.getUsername();
                String status= "pesan";

                if(!status_homestay.equals("Kosong")){
                    Toast.makeText(pemesanan.this, "Kamar Sedang Di Huni", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(nama_pemesan.equals("")||tanggal_checkin.equals("")||tanggal_checkout.equals("")){
                        Toast.makeText(pemesanan.this, "Field Cannnot Empty", Toast.LENGTH_SHORT).show();
                    }else{
                        String today = getToday();
                        int y = Integer.parseInt(today.substring(0,4));
                        int m = Integer.parseInt(today.substring(5,6));
                        int h = Integer.parseInt(today.substring(7));
                        int todays = ((y*366)+(m*31)+h);

                        String c = tanggal_checkin;
                        String tahun=c.substring(0,4);
                        String bulan=c.substring(5,6);
                        String hari = c.substring(7);
                        int thn=Integer.parseInt(tahun);
                        int bln=Integer.parseInt(bulan);
                        int day = Integer.parseInt(hari);

                        int masuk = ((thn*366)+(bln*31)+day);

                        int selisih  = masuk - todays;
                        //input tanggal check out
                        String d = tanggal_checkout;
                        int year = Integer.parseInt(d.substring(0,4));
                        int month = Integer.parseInt(d.substring(5,6));
                        int days = Integer.parseInt(d.substring(7));

                        int keluar = ((year*366)+(month*31)+days);

                        int total = (keluar -  masuk)*Integer.parseInt(harga);
                        if(masuk < todays || keluar < todays || keluar <= masuk){
                            Toast.makeText(getApplicationContext(), "Masukkan Tanggal Yang Valid", Toast.LENGTH_SHORT).show();
                        }
                        else if(selisih < 2){
                            Toast.makeText(getApplicationContext(), "Pemesanan harus dilakukan minimal 2 hari sebelum", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            progress = new ProgressDialog(pemesanan.this);
                            progress.setCancelable(false);
                            progress.setMessage("Loading ...");
                            progress.show();
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(URL)
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();

                            PesanAPI api = retrofit.create(PesanAPI.class);
                            Call<Value> call = api.daftar(id_homestay, nama_pemesan, tanggal_checkin, tanggal_checkout, total, homestays, username, status, owner);
                            call.enqueue(new Callback<Value>() {
                                @Override
                                public void onResponse(Call<Value> call, Response<Value> response) {
                                    String value = response.body().getValue();
                                    String massage = response.body().getMessage();
                                    progress.dismiss();
                                    if(value.equals("1")){
                                        Toast.makeText(pemesanan.this, massage, Toast.LENGTH_SHORT).show();
                                        startActivity(new Intent(pemesanan.this, MainActivity.class));
                                        finish();
                                    }
                                    else{
                                        Toast.makeText(pemesanan.this, massage, Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Value> call, Throwable t) {
                                    progress.dismiss();
                                    Log.e("masssage : ",t.getMessage());
                                    Toast.makeText(pemesanan.this, "Jaringan Error", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }

            }
        });
    }
    public String getToday(){
        DateFormat date = new SimpleDateFormat("yyyy-M-d");
        Date dates = new Date();
        return date.format(dates);
    }
}
