package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by test on 5/27/2019.
 */

public class HomestayDetailAdapter extends ArrayAdapter<HomestayModel> {
    private Context context;
    public HomestayDetailAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<HomestayModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.homestay_detail_items, parent, false
            );
        }

        Picasso.Builder picassoBuilder = new Picasso.Builder(context);
        Picasso picasso = picassoBuilder.build();
        final HomestayModel homestayModel = getItem(position);

        TextView nama_homestay = (TextView) convertView.findViewById(R.id.nama_homestay);
        nama_homestay.setText(homestayModel.getNama_homestay());

        TextView alamat = (TextView) convertView.findViewById(R.id.alamat);
        alamat.setText(homestayModel.getAlamat());

        TextView jumlah_kamar = (TextView) convertView.findViewById(R.id.jumlah_kamar);
        jumlah_kamar.setText(homestayModel.getJumlah_kamar());

        jumlah_kamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), KamarActivity.class);
                intent.putExtra("id_homestay", homestayModel.getId());
                intent.putExtra("owner", homestayModel.getOwner());
                intent.putExtra("kontak", homestayModel.getKontak());
                intent.putExtra("rekening", homestayModel.getNo_rekening());
                context.startActivity(intent);
            }
        });
        TextView lihat = (TextView) convertView.findViewById(R.id.lihat);
        lihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), KamarActivity.class);
                intent.putExtra("id_homestay", homestayModel.getId());
                intent.putExtra("owner", homestayModel.getOwner());
                intent.putExtra("kontak", homestayModel.getKontak());
                intent.putExtra("rekening", homestayModel.getNo_rekening());
                context.startActivity(intent);
            }
        });

        TextView jlh = (TextView) convertView.findViewById(R.id.jlh);
        jlh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), KamarActivity.class);
                intent.putExtra("id_homestay", homestayModel.getId());
                intent.putExtra("owner", homestayModel.getOwner());
                intent.putExtra("kontak", homestayModel.getKontak());
                intent.putExtra("rekening", homestayModel.getNo_rekening());
                context.startActivity(intent);
            }
        });
        TextView area = (TextView) convertView.findViewById(R.id.area);
        area.setText(homestayModel.getArea());

        TextView kontak = (TextView) convertView.findViewById(R.id.kontak);
        kontak.setText(homestayModel.getKontak());

        TextView rekening = (TextView) convertView.findViewById(R.id.rekening);
        rekening.setText(homestayModel.getNo_rekening());

        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);
        picasso.load("http://192.168.43.122/Homestay/public/images/"+homestayModel.getGambar()).into(imageView);

        TextView fasilitas = (TextView) convertView.findViewById(R.id.fasilitas);
        fasilitas.setText(homestayModel.getFasilitas());

        ImageView image = (ImageView) convertView.findViewById(R.id.gambar2);
        picasso.load("http://192.168.43.122/Homestay/public/images/"+homestayModel.getGambar_toilet()).into(image);


        Button pesan = (Button) convertView.findViewById(R.id.btnPemesanan);
        pesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(getContext(), pemesanan.class);
                mainIntent.putExtra("owner", homestayModel.getOwner());
                mainIntent.putExtra("status", homestayModel.getStatus_homestay());
                mainIntent.putExtra("id_homestay", homestayModel.getId());
                mainIntent.putExtra("nama_homestay",homestayModel.getNama_homestay());
                mainIntent.putExtra("harga", homestayModel.getHarga());
                mainIntent.putExtra("status_homestay", homestayModel.getStatus_homestay());
                context.startActivity(mainIntent);
            }
        });

        return convertView;
    }


}
