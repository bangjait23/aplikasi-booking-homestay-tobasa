package uts2019.develops.stud11417015.proyekakhir2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by test on 5/18/2019.
 */

public class PemesananModel {
    @SerializedName("nama_pemesan")
    @Expose
    private String nama_pemesan;
    @SerializedName("tanggal_checkin")
    @Expose
    private String checkin;
    @SerializedName("tanggal_checkout")
    @Expose
    private String checkout;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("nama_homestay")
    @Expose
    private String nama_homestay;


    public PemesananModel(String nama_pemesan, String checkin, String checkout, String status, String total, String nama_homestay) {
        this.nama_pemesan = nama_pemesan;
        this.checkin = checkin;
        this.checkout = checkout;
        this.status = status;
        this.total = total;
        this.nama_homestay = nama_homestay;
    }

    public String getNama_pemesan() {
        return nama_pemesan;
    }

    public void setNama_pemesan(String nama_pemesan) {
        this.nama_pemesan = nama_pemesan;
    }

    public String getCheckin() {
        return checkin;
    }

    public void setCheckin(String checkin) {
        this.checkin = checkin;
    }

    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String checkout) {
        this.checkout = checkout;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getNama_homestay() {
        return nama_homestay;
    }

    public void setNama_homestay(String nama_homestay) {
        this.nama_homestay = nama_homestay;
    }
}
