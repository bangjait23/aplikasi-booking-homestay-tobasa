package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by test on 6/9/2019.
 */

public class PemesananKamarAdapter extends ArrayAdapter<pesanKamarModel>{

    public PemesananKamarAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<pesanKamarModel> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.pemesanankamar_item, parent, false);
        }
        final pesanKamarModel pesan = getItem(position);
        TextView tanggalMasuk  = (TextView) convertView.findViewById(R.id.checkin);
        tanggalMasuk.setText(pesan.getTanggalMasuk());


        TextView tanggalKeluar = (TextView) convertView.findViewById(R.id.checkout);
        tanggalKeluar.setText(pesan.getTangalKeluar());

        TextView homestay = (TextView) convertView.findViewById(R.id.homestay);
        homestay.setText(pesan.getNama_homestay());

        TextView status = (TextView) convertView.findViewById(R.id.status);
        status.setText(pesan.getStatus());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), detailPemesananKamar.class);
                intent.putExtra("nama_homestay", pesan.getNama_homestay());
                intent.putExtra("no_kamar", pesan.getNo_kamar());
                intent.putExtra("tanggal_masuk",pesan.getTanggalMasuk());
                intent.putExtra("tanggal_keluar",pesan.getTangalKeluar());
                intent.putExtra("status",pesan.getStatus());
                intent.putExtra("total_pemba", pesan.getTotal());
                getContext().startActivity(intent);
            }
        });
        return  convertView;
    }
}
