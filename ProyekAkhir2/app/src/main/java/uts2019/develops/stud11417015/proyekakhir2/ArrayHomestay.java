package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by test on 5/9/2019.
 */

public class ArrayHomestay extends ArrayAdapter<HomestayModel> {
    private Context context;
    public ArrayHomestay(Context context,int resource,List<HomestayModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.homestay_items, parent, false
            );
        }
        Picasso.Builder picassoBulider = new Picasso.Builder(context);
        final HomestayModel home = getItem(position);

        TextView nama_homestay = (TextView) convertView.findViewById(R.id.nama_homestay);
        nama_homestay.setText(home.getNama_homestay());


        TextView alamat = (TextView) convertView.findViewById(R.id.alamat);
        alamat.setText(home.getAlamat());

        ImageView image = (ImageView) convertView.findViewById(R.id.image);
        Picasso picasso = picassoBulider.build();
        picasso.load("http://192.168.43.122/Homestay/public/images/"+home.getGambar()).into(image);



         convertView.setOnClickListener( new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent = new Intent(context, HomestayDetail.class);
                 intent.putExtra("id",home.getId());
                 intent.putExtra("nama_homestay", home.getNama_homestay());

                 context.startActivity(intent);
             }
         });
        return convertView;
    }
}
//http://localhost:8000/images/test.jpg