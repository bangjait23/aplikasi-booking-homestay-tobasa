package uts2019.develops.stud11417015.proyekakhir2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by test on 5/9/2019.
 */
public class HomestayModel {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("nama_homestay")
    @Expose
    private String nama_homestay;
    @SerializedName("status_homestay")
    @Expose
    private String status_homestay;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("jumlah_kamar")
    @Expose
    private String jumlah_kamar;
    @SerializedName("area_terdekat")
    @Expose
    private String area;
    @SerializedName("harga")
    @Expose
    private String harga;
    @SerializedName("gambar")
    @Expose
    private String gambar;
    @SerializedName("fasilitas")
    @Expose
    private String fasilitas;
    @SerializedName("gambar_toilet")
    @Expose
    private String gambar_toilet;
    @SerializedName("no_kamar")
    @Expose
    private String no_kamar;
    @SerializedName("jumlah_orang")
    @Expose
    private String jumlah_orang;
    @SerializedName("harga_kamar")
    @Expose
    private String harga_kamar;
    @SerializedName("gambar_kamar")
    @Expose
    private String gambar_kamar;
    @SerializedName("status")
    @Expose
    private String status_kamar;
    @SerializedName("Kontak")
    @Expose
    private String kontak;
    @SerializedName("no_rekening")
    @Expose
    private String no_rekening;
    @SerializedName("owner")
    @Expose
    private String owner;

    public HomestayModel(String id, String nama_homestay, String status_homestay, String alamat, String jumlah_kamar, String area, String harga, String gambar, String fasilitas, String gambar_toilet, String no_kamar, String jumlah_orang, String harga_kamar, String gambar_kamar, String status_kamar, String kontak, String no_rekening, String owner) {
        this.id = id;
        this.nama_homestay = nama_homestay;
        this.status_homestay = status_homestay;
        this.alamat = alamat;
        this.jumlah_kamar = jumlah_kamar;
        this.area = area;
        this.harga = harga;
        this.gambar = gambar;
        this.fasilitas = fasilitas;
        this.gambar_toilet = gambar_toilet;
        this.no_kamar = no_kamar;
        this.jumlah_orang = jumlah_orang;
        this.harga_kamar = harga_kamar;
        this.gambar_kamar = gambar_kamar;
        this.status_kamar = status_kamar;
        this.kontak = kontak;
        this.no_rekening = no_rekening;
        this.owner = owner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_homestay() {
        return nama_homestay;
    }

    public void setNama_homestay(String nama_homestay) {
        this.nama_homestay = nama_homestay;
    }

    public String getStatus_homestay() {
        return status_homestay;
    }

    public void setStatus_homestay(String status_homestay) {
        this.status_homestay = status_homestay;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getJumlah_kamar() {
        return jumlah_kamar;
    }

    public void setJumlah_kamar(String jumlah_kamar) {
        this.jumlah_kamar = jumlah_kamar;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getFasilitas() {
        return fasilitas;
    }

    public void setFasilitas(String fasilitas) {
        this.fasilitas = fasilitas;
    }

    public String getGambar_toilet() {
        return gambar_toilet;
    }

    public void setGambar_toilet(String gambar_toilet) {
        this.gambar_toilet = gambar_toilet;
    }

    public String getNo_kamar() {
        return no_kamar;
    }

    public void setNo_kamar(String no_kamar) {
        this.no_kamar = no_kamar;
    }

    public String getJumlah_orang() {
        return jumlah_orang;
    }

    public void setJumlah_orang(String jumlah_orang) {
        this.jumlah_orang = jumlah_orang;
    }

    public String getHarga_kamar() {
        return harga_kamar;
    }

    public void setHarga_kamar(String harga_kamar) {
        this.harga_kamar = harga_kamar;
    }

    public String getGambar_kamar() {
        return gambar_kamar;
    }

    public void setGambar_kamar(String gambar_kamar) {
        this.gambar_kamar = gambar_kamar;
    }

    public String getStatus_kamar() {
        return status_kamar;
    }

    public void setStatus_kamar(String status_kamar) {
        this.status_kamar = status_kamar;
    }

    public String getKontak() {
        return kontak;
    }

    public void setKontak(String kontak) {
        this.kontak = kontak;
    }

    public String getNo_rekening() {
        return no_rekening;
    }

    public void setNo_rekening(String no_rekening) {
        this.no_rekening = no_rekening;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}

