package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by test on 5/19/2019.
 */

public class WisataAdapter extends ArrayAdapter<wisataModel> {
    private Context context;
    public WisataAdapter(Context context, int resource, List<wisataModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.wisata_item, parent, false
            );
        }
        Picasso.Builder picassos = new Picasso.Builder(context);
        final wisataModel wisata = getItem(position);

        TextView nama = (TextView) convertView.findViewById(R.id.text);
        nama.setText(wisata.getNama_wisata());
        ImageView image = (ImageView) convertView.findViewById(R.id.image);

        Picasso picasso = picassos.build();
        picasso.load("http://192.168.43.122/Homestay/public/images/"+wisata.getGambar()).into(image);


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WisataDetail.class);
                intent.putExtra("nama_wisata", wisata.getNama_wisata());
                intent.putExtra("gambar",wisata.getGambar());

                context.startActivity(intent);
            }
        });

        return convertView;
    }
}
