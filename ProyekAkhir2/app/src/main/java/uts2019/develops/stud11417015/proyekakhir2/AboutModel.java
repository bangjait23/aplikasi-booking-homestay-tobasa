package uts2019.develops.stud11417015.proyekakhir2;

/**
 * Created by test on 5/23/2019.
 */

public class AboutModel {
    private String id;
    private String nama;
    private String email;
    private String kontak;
    private String alamat;
    private String jurusan;
    private String gambar;

    public AboutModel(String id, String nama, String email, String kontak, String alamat, String jurusan, String gambar) {
        this.id = id;
        this.nama = nama;
        this.email = email;
        this.kontak = kontak;
        this.alamat = alamat;
        this.jurusan = jurusan;
        this.gambar = gambar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKontak() {
        return kontak;
    }

    public void setKontak(String kontak) {
        this.kontak = kontak;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
