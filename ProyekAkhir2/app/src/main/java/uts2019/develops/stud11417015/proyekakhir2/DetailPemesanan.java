package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailPemesanan extends AppCompatActivity {
    TextView nama_customer, checkin, checkout, homestay, status1;
    Button kritik;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pemesanan);

        nama_customer = (TextView) findViewById(R.id.nama_customer);
        checkin = (TextView) findViewById(R.id.checkin);
        checkout = (TextView) findViewById(R.id.checkout);
        homestay = (TextView) findViewById(R.id.homestay);
        status1  = (TextView) findViewById(R.id.status);
        kritik = (Button) findViewById(R.id.kritik);

        Intent mainIntent = getIntent();
        final String customer = mainIntent.getExtras().getString("nama_customer");
        String checkIn = mainIntent.getExtras().getString("checkin");
        String checkOut = mainIntent.getExtras().getString("checkout");
        final String nama_homestay = mainIntent.getExtras().getString("homestay");
        String status = mainIntent.getExtras().getString("status");

        nama_customer.setText(customer);
        checkin.setText(checkIn);
        checkout.setText(checkOut);
        homestay.setText(nama_homestay);
        status1.setText(status);


        kritik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), KritikSaran.class);
                intent.putExtra("nama_customer", customer);
                intent.putExtra("homestay", nama_homestay);
                getApplicationContext().startActivity(intent);
            }
        });

    }
}
