package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomestayDetail extends AppCompatActivity {
    private Context context = HomestayDetail.this;
    GridView grid;
    Button pesan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homestay_detail);

        Intent intent = getIntent();
        String home = intent.getExtras().getString("nama_homestay");

        HomestayService homestayService = APIClient.getRetrofit().create(HomestayService.class);
        final Call<List<HomestayModel>> homestayCall = homestayService.getHomestay(home);

        homestayCall.enqueue(new Callback<List<HomestayModel>>() {
            @Override
            public void onResponse(Call<List<HomestayModel>> call, Response<List<HomestayModel>> response) {
                if(response.isSuccessful()){
                    tampil_homestay(response.body());
                }else{
                    Log.e("Error Message : ", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<HomestayModel>> call, Throwable t) {
                Toast.makeText(context, "Periksa Koneksi Anda", Toast.LENGTH_SHORT).show();
            }
        });

//        pesan = (Button) findViewById(R.id.btnPemesanan);
//        pesan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent mainIntent = new Intent(getApplicationContext(), pemesanan.class);
//                mainIntent.putExtra("nama_homestay",name);
//                startActivity(mainIntent);
//            }
//        });
    }
    public void tampil_homestay(List<HomestayModel> homestays){
        HomestayDetailAdapter homestayDetailAdapter = new HomestayDetailAdapter(getApplicationContext(), R.layout.homestay_detail_items, homestays);
        grid = (GridView) findViewById(R.id.gridView);
        grid.setAdapter(homestayDetailAdapter);
        homestayDetailAdapter.notifyDataSetChanged();
    }
}
