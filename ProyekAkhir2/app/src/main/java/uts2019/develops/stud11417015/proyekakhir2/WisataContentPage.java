package uts2019.develops.stud11417015.proyekakhir2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;
import android.widget.ImageView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WisataContentPage extends AppCompatActivity {
    GridView gridView;
    private int[] images = new int[]{
            R.drawable.carousel1, R.drawable.siboruon, R.drawable.pasir_putih, R.drawable.lumban_bulbul
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wisata_content_page);
        CarouselView carouselView = (CarouselView) findViewById(R.id.carousel);
        carouselView.setPageCount(images.length);
        carouselView.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                imageView.setImageResource(images[position]);
            }
        });
        HomestayService homestayService = APIClient.getRetrofit().create(HomestayService.class);
        final Call<List<wisataModel>> wisataCall = homestayService.getWisata();
        wisataCall.enqueue(new Callback<List<wisataModel>>() {
            @Override
            public void onResponse(Call<List<wisataModel>> call, Response<List<wisataModel>> response) {
                if(response.isSuccessful()){
                    tampilkanWisata(response.body());
                }else {
                    Log.e("Error Message", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<wisataModel>> call, Throwable t) {
                Log.e("Message", t.getMessage());
            }
        });
    }
    private void tampilkanWisata(List<wisataModel> wisata){
        WisataAdapter wisataAdapter = new WisataAdapter(getApplicationContext(), R.layout.wisata_item, wisata);
        gridView = (GridView)findViewById(R.id.gridView1);
        gridView.setAdapter(wisataAdapter);
        wisataAdapter.notifyDataSetChanged();
    }
}
