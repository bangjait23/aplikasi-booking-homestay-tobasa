package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

/**
 * Created by test on 5/18/2019.
 */

public class PemesananAdapter extends ArrayAdapter<PemesananModel> {


    public PemesananAdapter(Context context,  int resource,  List<PemesananModel> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.pemesanan_item, parent, false
            );
        }

        final PemesananModel pemesanan = getItem(position);
        DecimalFormat rupiah = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp.");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        rupiah.setDecimalFormatSymbols(formatRp);
        TextView nama_homestay = (TextView) convertView.findViewById(R.id.nama_pemesan);
        nama_homestay.setText(pemesanan.getNama_pemesan());

        TextView checkin = (TextView) convertView.findViewById(R.id.checkin);
        checkin.setText(pemesanan.getCheckin());

        TextView checkout = (TextView) convertView.findViewById(R.id.checkout);
        checkout.setText(pemesanan.getCheckout());


        TextView homestay = (TextView) convertView.findViewById(R.id.homestay);
        homestay.setText(pemesanan.getNama_homestay());

        TextView status = (TextView) convertView.findViewById(R.id.status);
        status.setText(pemesanan.getStatus());

        TextView total = (TextView) convertView.findViewById(R.id.total);
        total.setText(rupiah.format(Long.valueOf(pemesanan.getTotal())));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), DetailPemesanan.class);
                intent.putExtra("nama_customer", pemesanan.getNama_pemesan());
                intent.putExtra("checkin", pemesanan.getCheckin());
                intent.putExtra("checkout", pemesanan.getCheckout());
                intent.putExtra("homestay", pemesanan.getNama_homestay());
                intent.putExtra("status", pemesanan.getStatus());
                getContext().startActivity(intent);
            }
        });

        return convertView;
    }
}
