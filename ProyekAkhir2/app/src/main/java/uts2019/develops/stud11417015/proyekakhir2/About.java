package uts2019.develops.stud11417015.proyekakhir2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class About extends Fragment {
    GridView gridView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_about, container, false);

        HomestayService homestayService = APIClient.getRetrofit().create(HomestayService.class);
        final Call<List<AboutModel>> about = homestayService.getAdmin();

        about.enqueue(new Callback<List<AboutModel>>() {
            @Override
            public void onResponse(Call<List<AboutModel>> call, Response<List<AboutModel>> response) {
                if(response.isSuccessful()){
                    tampilAdmin(response.body());
                }
                else {
                    Log.e("Error : ", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<AboutModel>> call, Throwable t) {
                Log.e("Error : ", t.getMessage());
            }
        });


        return view;
    }
    public void tampilAdmin(List<AboutModel> about){
        ArrayAdapter arrayAdapter = new AboutAdapter(getContext(), R.layout.about_item, about);
        gridView = (GridView) getActivity().findViewById(R.id.gridView1);
        gridView.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();
    }
}
