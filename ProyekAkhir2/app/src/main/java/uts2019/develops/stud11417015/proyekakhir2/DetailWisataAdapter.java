package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by test on 5/19/2019.
 */

public class DetailWisataAdapter extends ArrayAdapter<wisataModel> {
    private Context context;
    public DetailWisataAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<wisataModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.detail_wisata_item, parent, false
            );
        }
        Picasso.Builder picassos = new Picasso.Builder(context);
        final wisataModel wisata = getItem(position);

        TextView nama = (TextView) convertView.findViewById(R.id.nama);
        nama.setText(wisata.getNama_wisata());
        TextView alamat = (TextView) convertView.findViewById(R.id.alamat);
        alamat.setText(wisata.getAlamat());
        TextView deskripsi = (TextView) convertView.findViewById(R.id.deskripsi);
        deskripsi.setText(wisata.getDeskripsi());
        ImageView image = (ImageView) convertView.findViewById(R.id.imageView);


        Picasso picasso = picassos.build();
        picasso.load("http://192.168.43.122/Homestay/public/images/"+wisata.getGambar()).into(image);



        return convertView;
    }
}
