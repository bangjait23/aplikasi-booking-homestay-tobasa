package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

/**
 * Created by test on 6/7/2019.
 */

public class KamarAdapter extends ArrayAdapter<KamarModel> {
    private Context context;
    public KamarAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<KamarModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(
              R.layout.kamar_items,parent, false
            );
        }
        DecimalFormat rupiah = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp.");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        rupiah.setDecimalFormatSymbols(formatRp);

        Picasso.Builder picassoBuilder = new Picasso.Builder(context);
        Picasso picasso = picassoBuilder.build();

        final KamarModel homestayModel = getItem(position);

        ImageView gambar_kamar = (ImageView) convertView.findViewById(R.id.gambar_kamar);
        picasso.load("http://192.168.43.122/Homestay/public/images/"+homestayModel.getGambar_kamar()).into(gambar_kamar);

        TextView harga_kamar = (TextView) convertView.findViewById(R.id.harga_kamar);
        harga_kamar.setText(rupiah.format(Long.valueOf(homestayModel.getHarga_kamar())));

        TextView jumlah_orang = (TextView) convertView.findViewById(R.id.jumlah_orang);
        jumlah_orang.setText(homestayModel.getJumlah_orang());

        TextView status_kamar = (TextView) convertView.findViewById(R.id.available);
        status_kamar.setText(homestayModel.getStatus_kamar());

        TextView nomor = (TextView) convertView.findViewById(R.id.no_kamar);
        nomor.setText(homestayModel.getNo_kamar());

        Button booking = (Button) convertView.findViewById(R.id.bookingKamar);
        booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!homestayModel.getStatus_kamar().equals("Available")){
                    Toast.makeText(context, "Kamar Sudah Di Booking", Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent intent = new Intent(getContext(), PesanKamar.class);
                    intent.putExtra("id_homestay", homestayModel.getId_homestay());
                    intent.putExtra("nama_homestay", homestayModel.getNama_homestay());
                    intent.putExtra("no_kamar", homestayModel.getNo_kamar());
                    intent.putExtra("id_kamar", homestayModel.getId());
                    intent.putExtra("gambar_kamar", homestayModel.getGambar_kamar());
                    intent.putExtra("harga", homestayModel.getHarga_kamar());
                    intent.putExtra("status", homestayModel.getStatus_kamar());
                    context.startActivity(intent);
                }
            }
        });

        return convertView;
    }
}
