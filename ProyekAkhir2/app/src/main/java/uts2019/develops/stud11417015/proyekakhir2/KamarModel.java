package uts2019.develops.stud11417015.proyekakhir2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by test on 6/7/2019.
 */

public class KamarModel {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("id_homestay")
    @Expose
    private String id_homestay;
    @SerializedName("nama_homestay")
    @Expose
    private String nama_homestay;
    @SerializedName("no_kamar")
    @Expose
    private String no_kamar;
    @SerializedName("jumlah_orang")
    @Expose
    private String jumlah_orang;
    @SerializedName("harga_kamar")
    @Expose
    private String harga_kamar;
    @SerializedName("gambar_kamar")
    @Expose
    private String gambar_kamar;
    @SerializedName("status")
    @Expose
    private String status_kamar;

    public KamarModel(String id, String id_homestay, String nama_homestay, String no_kamar, String jumlah_orang, String harga_kamar, String gambar_kamar, String status_kamar) {
        this.id = id;
        this.id_homestay = id_homestay;
        this.nama_homestay = nama_homestay;
        this.no_kamar = no_kamar;
        this.jumlah_orang = jumlah_orang;
        this.harga_kamar = harga_kamar;
        this.gambar_kamar = gambar_kamar;
        this.status_kamar = status_kamar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_homestay() {
        return id_homestay;
    }

    public void setId_homestay(String id_homestay) {
        this.id_homestay = id_homestay;
    }

    public String getNama_homestay() {
        return nama_homestay;
    }

    public void setNama_homestay(String nama_homestay) {
        this.nama_homestay = nama_homestay;
    }

    public String getNo_kamar() {
        return no_kamar;
    }

    public void setNo_kamar(String no_kamar) {
        this.no_kamar = no_kamar;
    }

    public String getJumlah_orang() {
        return jumlah_orang;
    }

    public void setJumlah_orang(String jumlah_orang) {
        this.jumlah_orang = jumlah_orang;
    }

    public String getHarga_kamar() {
        return harga_kamar;
    }

    public void setHarga_kamar(String harga_kamar) {
        this.harga_kamar = harga_kamar;
    }

    public String getGambar_kamar() {
        return gambar_kamar;
    }

    public void setGambar_kamar(String gambar_kamar) {
        this.gambar_kamar = gambar_kamar;
    }

    public String getStatus_kamar() {
        return status_kamar;
    }

    public void setStatus_kamar(String status_kamar) {
        this.status_kamar = status_kamar;
    }
}
