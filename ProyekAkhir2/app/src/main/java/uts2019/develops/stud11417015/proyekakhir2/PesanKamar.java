package uts2019.develops.stud11417015.proyekakhir2;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PesanKamar extends AppCompatActivity {
    public static final String URL = "http://10.114.108.231/PesanHomestay/";
    private Context context = PesanKamar.this;
    TextView kontak, rekening;
    DatePickerDialog datePickerDialog;
    Button booking;
    EditText tanggal_masuk, tanggal_keluar, nama;
    private ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesan_kamar);
        Picasso.Builder picasso = new Picasso.Builder(context);
        Picasso p = picasso.build();
        Intent mainIntent = getIntent();
        final int id_homestay = Integer.parseInt(mainIntent.getExtras().getString("id_homestay"));
        final String nama_home = mainIntent.getExtras().getString("nama_homestay");
        final int no_kamar = Integer.parseInt(mainIntent.getExtras().getString("no_kamar"));
        final int id_kamar = Integer.parseInt(mainIntent.getExtras().getString("id_kamar"));
        final String status_kamar = mainIntent.getExtras().getString("status");
        String gambar = mainIntent.getExtras().getString("gambar_kamar");
        final String harga = mainIntent.getExtras().getString("harga");
        final String status = "Pesan";

        kontak = (TextView) findViewById(R.id.kontak);
        kontak.setText(KamarActivity.getKontak());

        rekening = (TextView) findViewById(R.id.rekening);
        rekening.setText(KamarActivity.getRekening());

        ImageView gambar_kamar = (ImageView) findViewById(R.id.imageKamar);
        p.load("http://192.168.43.122/Homestay/public/images/"+gambar).into(gambar_kamar);



        //DatePickerDialog
        tanggal_masuk = (EditText) findViewById(R.id.tanggal_masuk);
        tanggal_masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

                datePickerDialog = new DatePickerDialog(PesanKamar.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                tanggal_masuk.setText(year + "-"
                                        + (month + 1) + "-" + dayOfMonth);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        tanggal_keluar = (EditText) findViewById(R.id.tanggal_keluar);
        tanggal_keluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

                datePickerDialog = new DatePickerDialog(PesanKamar.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                tanggal_keluar.setText(year + "-"
                                        + (month + 1) + "-" + dayOfMonth);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        nama = (EditText) findViewById(R.id.nama_pemesan);
        booking = (Button) findViewById(R.id.buking);
        booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tanggal_keluar.getText().toString().equals("") || tanggal_masuk.getText().toString().equals("")){
                    Toast.makeText(context, "Form Harus Diisi", Toast.LENGTH_SHORT).show();
                }

                else{
                    //Tanggal hari ini
                    String today = getToday();
                    int y = Integer.parseInt(today.substring(0,4));
                    int m = Integer.parseInt(today.substring(5,6));
                    int h = Integer.parseInt(today.substring(7));
                    int todays = ((y*366)+(m*31)+h);

                    //Input tanggal checkin
                    String c = tanggal_masuk.getText().toString();
                    String tahun=c.substring(0,4);
                    String bulan=c.substring(5,6);
                    String hari = c.substring(7);
                    int thn=Integer.parseInt(tahun);
                    int bln=Integer.parseInt(bulan);
                    int day = Integer.parseInt(hari);

                    int masuk = ((thn*366)+(bln*31)+day);

                    int selisih  = masuk - todays;
                    //input tanggal check out
                    String d = tanggal_keluar.getText().toString();
                    int year = Integer.parseInt(d.substring(0,4));
                    int month = Integer.parseInt(d.substring(5,6));
                    int days = Integer.parseInt(d.substring(7));

                    int keluar = ((year*366)+(month*31)+days);

                    if(masuk < todays || keluar < todays || keluar <= masuk){
                        Toast.makeText(context, "Masukkan Tanggal Yang Valid", Toast.LENGTH_SHORT).show();
                    }
                    else if(selisih < 2){
                        Toast.makeText(context, "Pemesanan harus dilakukan minimal 2 hari sebelum", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        final int total = (keluar-masuk)*Integer.parseInt(harga);
                        progress = new ProgressDialog(PesanKamar.this);
                        progress.setCancelable(false);
                        progress.setMessage("Loading ...");
                        progress.show();
                        Retrofit retrofit = new Retrofit.Builder().baseUrl(URL).
                                addConverterFactory(GsonConverterFactory.create()).
                                build();
                        PesanAPI api = retrofit.create(PesanAPI.class);
                        Call<Value> call = api.pesanKamar(nama.getText().toString(), nama_home, id_homestay, no_kamar, id_kamar, status,total, tanggal_masuk.getText().toString(), tanggal_keluar.getText().toString(), Login.getUsername(), KamarActivity.getOwner());
                        call.enqueue(new Callback<Value>() {
                            @Override
                            public void onResponse(Call<Value> call, Response<Value> response) {
                                String value = response.body().getValue();
                                String massage = response.body().getMessage();
                                progress.dismiss();
                                if(value.equals("1")){
                                    Toast.makeText(PesanKamar.this, massage, Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(PesanKamar.this, MainActivity.class));
                                    finish();
                                }
                                else{
                                    Toast.makeText(PesanKamar.this, massage, Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Value> call, Throwable t) {
                                progress.dismiss();
                                Log.e("masssage : ",t.getMessage());
                                Toast.makeText(PesanKamar.this, "Jaringan Error", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }
        });
    }
    public String getToday(){
        DateFormat date = new SimpleDateFormat("yyyy-M-d");
        Date dates = new Date();
        return date.format(dates);
    }
}
