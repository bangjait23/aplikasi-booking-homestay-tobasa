package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private Fragment fragments = null;
    private FragmentManager fragmentManager;
    DrawerLayout drawer;
    public static String homestays;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        Intent mainIntent = getIntent();
//        String username = mainIntent.getExtras().getString("username");
//        getUsername(username);



        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Make CarouselgetSupportFragmentManager().beginTransaction().replace(R.id.fragment, new HomeFragment()).commit();
        getSupportFragmentManager().beginTransaction().
                replace(R.id.fragment, new FragmentHome()).
                commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
       if (id == R.id.homestay) {
            getSupportFragmentManager().beginTransaction().
                    replace(R.id.fragment, new FragmentHome()).
                    commit();
        } else if (id == R.id.pemesanan) {
            getSupportFragmentManager().beginTransaction().
                    replace(R.id.fragment, new ListPemesanan()).commit();
        } else if (id == R.id.wisata) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment, new DaftarWisata())
                    .commit();
        }else if(id == R.id.pemesanan_kamar){
           getSupportFragmentManager().beginTransaction()
                   .replace(R.id.fragment, new PemesananKamar()).commit();
       }
        else if (id == R.id.info) {
            Uri uri = Uri.parse("http://www.go_jek.com");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
        else {
            if (id == R.id.nav_share) {
                startActivity(new Intent(MainActivity.this, HomepageContent.class));
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//         getMenuInflater().inflate(R.menu.search_menu, menu);
//        return true;
//    }
    private void displayView(int position) {
        fragments = null;
        String fragmentTags = "";
        switch (position) {
            case 0:
                fragments = new FragmentHome();
                break;

            default:
                break;
        }

        if (fragments != null) {
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment, fragments, fragmentTags).commit();
        }
    }
}
