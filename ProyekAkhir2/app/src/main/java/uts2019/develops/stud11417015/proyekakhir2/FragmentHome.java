package uts2019.develops.stud11417015.proyekakhir2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentHome extends Fragment implements SearchView.OnQueryTextListener, MenuItem.OnActionExpandListener  {
    ListView listView;
    String nama_homestay = "";
    private FragmentActivity mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        setHasOptionsMenu(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_home, container, false);

        HomestayService homestayService = APIClient.getRetrofit().create(HomestayService.class);
        final Call<List<HomestayModel>> homestayCall = homestayService.getHome();
        homestayCall.enqueue(new Callback<List<HomestayModel>>() {
            @Override
            public void onResponse(Call<List<HomestayModel>> call, Response<List<HomestayModel>> response) {
                if (response.isSuccessful()) {
                    tampilkan(response.body());
                } else {
                    Log.e("Error Message", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<HomestayModel>> call, Throwable t) {
                Log.e("Message", t.getMessage());
            }
        });
        loadhomestay("");
        return view;
    }

    public void loadhomestay(String homestay) {
        HomestayService homestayService = APIClient.getRetrofit().create(HomestayService.class);
        final Call<List<HomestayModel>> homestayCall = homestayService.getHomestay(homestay);

        homestayCall.enqueue(new Callback<List<HomestayModel>>() {
            @Override
            public void onResponse(Call<List<HomestayModel>> call, Response<List<HomestayModel>> response) {
                if (response.isSuccessful()) {
                    tampilkan(response.body());
                } else {
                    Log.e("Error Message", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<HomestayModel>> call, Throwable t) {
                Log.e("Message", t.getMessage());
            }
        });
    }

    private void tampilkan(List<HomestayModel> home) {
        ArrayHomestay arrayHomestay = new ArrayHomestay(getContext(), R.layout.homestay_items, home);
        listView = (ListView) getActivity().findViewById(R.id.list_buku);
        listView.setAdapter(arrayHomestay);
        arrayHomestay.notifyDataSetChanged();
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.menuSearch);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("Search");

        super.onCreateOptionsMenu(menu, inflater);

        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onQueryTextSubmit(String query) {
        loadhomestay(query);
        return true;
    }
    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText == null || newText.trim().isEmpty()) {
            loadhomestay("");
            return true;
        }
        return false;
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return false;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        return false;
    }
    public interface OnItem1SelectedListener {
        void OnItem1SelectedListener(String item);
    }
}
