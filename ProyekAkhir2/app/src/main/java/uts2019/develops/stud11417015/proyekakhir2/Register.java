package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends AppCompatActivity {
    DatabaseHelper db;
    EditText name, username, pass, no_ktp, alamat;
    Button register;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        db = new DatabaseHelper(this);
        name = (EditText) findViewById(R.id.name);
        username = (EditText) findViewById(R.id.username);
        pass = (EditText) findViewById(R.id.password);
        no_ktp = (EditText) findViewById(R.id.no_ktp);
        alamat = (EditText) findViewById(R.id.alamat);

        register = (Button) findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama = name.getText().toString();
                String uname = username.getText().toString();
                String pwd = pass.getText().toString();
                String ktp = no_ktp.getText().toString();
                String address = alamat.getText().toString();

                if(nama.equals("")||uname.equals("")||pwd.equals("")||ktp.equals("")||address.equals("")){
                    Toast.makeText(Register.this, "Fields Are empty", Toast.LENGTH_SHORT).show();
                }
                else{
                    Boolean checkEmail = db.checkEmail(uname);
                    if(checkEmail==true){
                        Boolean insert = db.insert(uname, pwd, nama, ktp, address);
                        if(insert==true){
                            startActivity(new Intent(Register.this, Login.class));
                            Toast.makeText(Register.this, "Registered Successfully", Toast.LENGTH_SHORT).show();
                         }
                    }
                    else{
                        Toast.makeText(Register.this, "Email Already Exists", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
