package uts2019.develops.stud11417015.proyekakhir2;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by test on 5/16/2019.
 */

public interface PesanAPI {
    @FormUrlEncoded
    @POST("insert.php")
    Call<Value> daftar(@Field("id_homestay") int id_homestay,
                       @Field("nama_pemesan") String nama_pemesan,
                       @Field("tanggal_checkin") String tanggal_checkin,
                       @Field("tanggal_checkout") String tanggal_checkout,
                       @Field("total") int total,
                       @Field("nama_homestay") String nama_homestay,
                       @Field("username") String username,
                       @Field("status") String status,
                       @Field("owner") String owner);
    @FormUrlEncoded
    @POST("addKritikSaran.php")
    Call<Value> saran(@Field("nama_homestay") String nama_homestay,
                      @Field("nama_customer") String nama_customer,
                      @Field("kritik") String kritik,
                      @Field("saran") String saran,
                      @Field("username") String username);

    @FormUrlEncoded
    @POST("pesanKamar.php")
    Call<Value> pesanKamar(@Field("nama_pemesan") String nama_pemesan,
                           @Field("nama_homestay") String nama_homestay,
                           @Field("id_homestay") int id_homestay,
                           @Field("no_kamar") int no_kamar,
                           @Field("id_kamar") int id_kamar,
                           @Field("status") String status,
                           @Field("total_pembayaran") int total,
                           @Field("tanggal_masuk") String tanggalMasuk,
                           @Field("tanggal_keluar") String tanggalKeluar,
                           @Field("username_pemesan") String username,
                           @Field("owner") String owne1r);
}
