package uts2019.develops.stud11417015.proyekakhir2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class KritikSaran extends AppCompatActivity {
    public static final String URL = "http://192.168.43.122/PesanHomestay/";
    private ProgressDialog progress;
    TextView customer, home;
    EditText kritik, saran;
    Button addKritik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kritik_saran);

        home = (TextView) findViewById(R.id.homestay);
        customer = (TextView) findViewById(R.id.nama_customer);
        kritik = (EditText) findViewById(R.id.kritik);
        saran = (EditText) findViewById(R.id.saran);

        Intent mainIntent = getIntent();
        final String nama = mainIntent.getExtras().getString("nama_customer");
        final String homestay = mainIntent.getExtras().getString("homestay");
        final String username = Login.getUsername();

        home.setText(homestay);
        customer.setText(nama);

        addKritik = (Button) findViewById(R.id.btnAdd);
        addKritik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress = new ProgressDialog(KritikSaran.this);
                progress.setCancelable(false);
                progress.setMessage("Loading ...");
                progress.show();

                String pendapat = kritik.getText().toString().trim();
                String masukan = saran.getText().toString().trim();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(URL).addConverterFactory(GsonConverterFactory.create())
                        .build();

                PesanAPI pesanAPI = retrofit.create(PesanAPI.class);
                Call<Value> call = pesanAPI.saran(homestay, nama, pendapat, masukan, username);
                call.enqueue(new Callback<Value>() {
                    @Override
                    public void onResponse(Call<Value> call, Response<Value> response) {
                        String value = response.body().getValue();
                        String massage = response.body().getMessage();
                        progress.dismiss();
                        if(value.equals("1")){
                            Toast.makeText(KritikSaran.this, massage, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(KritikSaran.this, MainActivity.class));
                        }
                        else{
                            Toast.makeText(KritikSaran.this, massage, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Value> call, Throwable t) {
                        progress.dismiss();
                        Log.e("message : ",t.getMessage());
                        Toast.makeText(KritikSaran.this, "Jaringan Error", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

    }
}
