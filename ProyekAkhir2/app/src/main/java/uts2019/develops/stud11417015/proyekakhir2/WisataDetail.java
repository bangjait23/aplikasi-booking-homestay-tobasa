package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.GridView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WisataDetail extends AppCompatActivity {
    GridView gridView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wisata_detail);

        Intent intent = getIntent();
        final String nama_wisata = intent.getExtras().getString("nama_wisata");
        Log.d("Nama Wisata : ", nama_wisata);

        HomestayService homestayService = APIClient.getRetrofit().create(HomestayService.class);
        final Call<List<wisataModel>> call = homestayService.getDetailWisata(nama_wisata);

        call.enqueue(new Callback<List<wisataModel>>() {
            @Override
            public void onResponse(Call<List<wisataModel>> call, Response<List<wisataModel>> response) {
                if(response.isSuccessful()){
                    tampilDetail(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<wisataModel>> call, Throwable t) {

            }
        });
    }
    public void tampilDetail(List<wisataModel> detail){
        DetailWisataAdapter detailWisataAdapter = new DetailWisataAdapter(getApplicationContext(), R.layout.detail_wisata_item, detail);
        gridView = (GridView) findViewById(R.id.gridView);
        gridView.setAdapter(detailWisataAdapter);
        detailWisataAdapter.notifyDataSetChanged();
    }
}
