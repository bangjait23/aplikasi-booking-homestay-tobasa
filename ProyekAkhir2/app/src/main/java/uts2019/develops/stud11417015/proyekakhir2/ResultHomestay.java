package uts2019.develops.stud11417015.proyekakhir2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by test on 5/9/2019.
 */

public class ResultHomestay {
    @SerializedName("Homestay")
    @Expose
    private List<HomestayModel> home = null;
    @SerializedName("pemesanan")
    @Expose
    private List<PemesananModel> pemesanan = null;

    @SerializedName("wisata")
    @Expose
    private List<wisataModel> wisata = null;

    public List<HomestayModel> gethome() {
        return home;
    }

    public List<PemesananModel> getPemesanan() {
        return pemesanan;
    }

    public List<wisataModel> getWisata(){
        return wisata;
    }
}
