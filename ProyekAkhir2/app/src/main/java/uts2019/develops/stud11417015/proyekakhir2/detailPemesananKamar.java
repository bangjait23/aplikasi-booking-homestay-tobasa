package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class detailPemesananKamar extends AppCompatActivity {
    TextView nama, no, masuk, keluar, state, total;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pemesanan_kamar);

        Intent mainIntent = getIntent();
        String nama_homestay = mainIntent.getExtras().getString("nama_homestay");
        String nomor = mainIntent.getExtras().getString("no_kamar");
        String in = mainIntent.getExtras().getString("tanggal_masuk");
        String out = mainIntent.getExtras().getString("tanggal_keluar");
        String status = mainIntent.getExtras().getString("status");
        String totals = mainIntent.getExtras().getString("total_pemba");

        DecimalFormat rupiah = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp.");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        rupiah.setDecimalFormatSymbols(formatRp);
        nama = (TextView) findViewById(R.id.nameHomestay);
        nama.setText(nama_homestay);

        no = (TextView) findViewById(R.id.nomor);
        no.setText(nomor);

        masuk = (TextView) findViewById(R.id.datang);
        masuk.setText(in);

        keluar = (TextView) findViewById(R.id.pulang);
        keluar.setText(out);

        state = (TextView) findViewById(R.id.state);
        state.setText(status);

        total = (TextView) findViewById(R.id.total_pembayaran);
        total.setText(rupiah.format(Long.valueOf(totals)));
    }
}
