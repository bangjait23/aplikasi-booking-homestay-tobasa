package uts2019.develops.stud11417015.proyekakhir2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.GridView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KamarActivity extends AppCompatActivity {
    private Context context = KamarActivity.this;
    public static String kontak;
    public static String rekening, owner;
    GridView grid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kamar);

        Intent intent = getIntent();
        String id_homestay = intent.getExtras().getString("id_homestay");
        kontak = intent.getExtras().getString("kontak");
        rekening = intent.getExtras().getString("rekening");
        owner = intent.getExtras().getString("owner");


        HomestayService homestayService = APIClient.getRetrofit().create(HomestayService.class);
        final Call<List<KamarModel>> kamarCall = homestayService.viewKamarHomestay(id_homestay);

        kamarCall.enqueue(new Callback<List<KamarModel>>() {
            @Override
            public void onResponse(Call<List<KamarModel>> call, Response<List<KamarModel>> response) {
                if(response.isSuccessful()){
                    tampil_Kamar(response.body());
                }
                else{
                    Toast.makeText(KamarActivity.this, "Periksa Koneksi Jaringan Anda", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<KamarModel>> call, Throwable t) {
                Toast.makeText(context, "Message : "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void tampil_Kamar(List<KamarModel> body) {
        KamarAdapter kamarAdapter = new KamarAdapter(getApplicationContext(), R.layout.kamar_items, body);
        grid = (GridView) findViewById(R.id.kamarGrid);
        grid.setAdapter(kamarAdapter);
        kamarAdapter.notifyDataSetChanged();
    }
    public static String getKontak(){
        return kontak;
    }
    public static String getRekening(){
        return rekening;
    }
    public static String getOwner(){
        return owner;
    }
}
