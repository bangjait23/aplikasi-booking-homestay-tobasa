package uts2019.develops.stud11417015.proyekakhir2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by test on 4/22/2019.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private Context context;
    public DatabaseHelper(Context context) {
        super(context, "bogasta.db", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
         db.execSQL("Create table user (username TEXT PRIMARY KEY, password TEXT, name TEXT, no_ktp TEXT, alamat TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists user");
    }
    public boolean insert(String username, String password, String name, String no_ktp, String alamat){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("username",username);
        cv.put("password",password);
        cv.put("name", name);
        cv.put("no_ktp",no_ktp);
        cv.put("alamat",alamat);
        long ins = db.insert("user", null, cv);
        if(ins==-1)return false;
        else return true;
    }
    public boolean checkEmail(String username){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user where username = ?", new String[]{username});
        if(cursor.getCount()>0)return false;
        else return true;
    }
    public Boolean login(String username, String password){
        Log.e("input", username + "," + password);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from user where username = ? and password = ?", new String[]{username, password});
        if(cursor.getCount()>0){
            return true;

//            Intent intent = new Intent(context, pemesanan.class);
//            intent.putExtra("username", username);
//            context.startActivity(intent);
        }
        else return false;


    }
}
