package uts2019.develops.stud11417015.proyekakhir2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListPemesanan extends Fragment {
    ListView listView;
    public static String Extra = "extra";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_pemesanan, container, false);


        HomestayService homestayService = APIClient.getRetrofit().create(HomestayService.class);
        final Call<List<PemesananModel>> pemesananCall = homestayService.getPemesanan(Login.getUsername());

        pemesananCall.enqueue(new Callback<List<PemesananModel>>() {
            @Override
            public void onResponse(Call<List<PemesananModel>> call, Response<List<PemesananModel>> response) {
                if (response.isSuccessful()) {
                    tampilPemesanan(response.body());
                } else {
                    Log.e("Error Massage : ", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<PemesananModel>> call, Throwable t) {
                Log.e("Error : ", t.getMessage());
            }
        });
        return view;
    }

    public void tampilPemesanan(List<PemesananModel> pemesananModels) {
        PemesananAdapter pemesananAdapter = new PemesananAdapter(getContext(), R.layout.pemesanan_item, pemesananModels);
        listView = (ListView) getActivity().findViewById(R.id.list_pemesanan);
        listView.setAdapter(pemesananAdapter);
        pemesananAdapter.notifyDataSetChanged();
    }
}
